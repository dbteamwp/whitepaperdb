﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void Login_dipedendente_Click(object sender, EventArgs e)
        {
            this.Hide();
            new loginDipendente().ShowDialog();
            this.Show();
        }

        private void ClienteButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            var cliente = new LoginCliente();
            cliente.ShowDialog();
            this.Show();
        }
    }
}
