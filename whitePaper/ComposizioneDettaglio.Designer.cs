﻿namespace whitePaper
{
    partial class ComposizioneDettaglio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.quantityBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.urlBox = new System.Windows.Forms.TextBox();
            this.confermaDettaglioButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.quantityBox)).BeginInit();
            this.SuspendLayout();
            // 
            // quantityBox
            // 
            this.quantityBox.Location = new System.Drawing.Point(272, 43);
            this.quantityBox.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.quantityBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quantityBox.Name = "quantityBox";
            this.quantityBox.Size = new System.Drawing.Size(120, 31);
            this.quantityBox.TabIndex = 0;
            this.quantityBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(167, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quantità:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(108, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "URL immagine:";
            // 
            // urlBox
            // 
            this.urlBox.Location = new System.Drawing.Point(272, 90);
            this.urlBox.MaxLength = 40;
            this.urlBox.Name = "urlBox";
            this.urlBox.Size = new System.Drawing.Size(484, 31);
            this.urlBox.TabIndex = 3;
            // 
            // confermaDettaglioButton
            // 
            this.confermaDettaglioButton.Location = new System.Drawing.Point(272, 160);
            this.confermaDettaglioButton.Name = "confermaDettaglioButton";
            this.confermaDettaglioButton.Size = new System.Drawing.Size(264, 92);
            this.confermaDettaglioButton.TabIndex = 4;
            this.confermaDettaglioButton.Text = "Conferma";
            this.confermaDettaglioButton.UseVisualStyleBackColor = true;
            this.confermaDettaglioButton.Click += new System.EventHandler(this.confermaDettaglioButton_Click);
            // 
            // ComposizioneDettaglio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(840, 298);
            this.Controls.Add(this.confermaDettaglioButton);
            this.Controls.Add(this.urlBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.quantityBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ComposizioneDettaglio";
            this.Text = "Componi dettaglio";
            ((System.ComponentModel.ISupportInitialize)(this.quantityBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown quantityBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox urlBox;
        private System.Windows.Forms.Button confermaDettaglioButton;
    }
}