﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class PagamentiForm : Form
    {
        private int id;
        private string tipo;
        public PagamentiForm(int id, string tipo)
        {
            InitializeComponent();
            this.id = id;
            this.tipo = tipo;
        }

        private void PagamentiForm_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            if(tipo == "D")
            {
                dataGridView1.DataSource = context.PAGAMENTOs
                .Where(p => p.FATTURA.ORDINE.idDipendente == this.id
                && p.dataPagamento == null).Select(p => new { p.idFattura, p.importo, p.dataScadenza }).OrderByDescending(p => p.dataScadenza);
            }
            else if (tipo == "A")
            {
                dataGridView1.DataSource = context.PAGAMENTOs
                .Where(p => p.dataPagamento == null).Select(p => new { p.idFattura, p.importo, p.dataScadenza }).OrderByDescending(p => p.dataScadenza);
            }
            else if (tipo == "C")
            {
                dataGridView1.DataSource = context.PAGAMENTOs
                .Where(p => p.FATTURA.ORDINE.idCliente == this.id
                && p.dataPagamento == null).Select(p => new { p.idFattura, p.importo, p.dataScadenza }).OrderByDescending(p => p.dataScadenza);
            }
            DataGridViewButtonColumn ordine = new DataGridViewButtonColumn();
            ordine.Name = "dataGridViewOrdineButton";
            ordine.Text = "ordine";
            ordine.HeaderText = "ordine";
            ordine.Width = 20;
            ordine.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            ordine.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(ordine);
            dataGridView1.CellClick += DataGridView1_CellClick;
            if (tipo == "A" || tipo == "D")
            {
                DataGridViewButtonColumn cliente = new DataGridViewButtonColumn();
                cliente.Name = "dataGridViewClienteButton";
                cliente.Text = "cliente";
                cliente.HeaderText = "cliente";
                cliente.Width = 20;
                cliente.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                cliente.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(cliente);
            }
            if(tipo == "A")
            {
                DataGridViewButtonColumn dipendente = new DataGridViewButtonColumn();
                dipendente.Name = "dataGridViewDipendenteButton";
                dipendente.Text = "dipendente";
                dipendente.HeaderText = "dipendente";
                dipendente.Width = 20;
                dipendente.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dipendente.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(dipendente);
            }
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3 && e.ColumnIndex != 4 && e.ColumnIndex != 5) { return; }
            if (e.RowIndex < 0) return;
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            int idOrdine = Convert.ToInt32(context.FATTURAs
                    .Where(f => f.idFattura == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value))
                    .Select(f => new { f.idOrdine }).First().idOrdine);
            if (e.ColumnIndex == 3 && (tipo == "D" || tipo == "A"))
            {
                new DettagliOrdine(idOrdine, "D").ShowDialog();
                this.Dispose();
            }
            else if (e.ColumnIndex == 3 && tipo == "C")
            {
                new DettagliOrdine(idOrdine, "C").ShowDialog();
                this.Dispose();
            }
            else if(e.ColumnIndex == 4 && (tipo == "D" || tipo == "A"))
            {
                new DettagliCliente(Convert.ToInt32(context.ORDINEs
                    .Where(o => o.idOrdine == idOrdine)
                    .Select(o => new { o.idCliente })
                    .First().idCliente)).ShowDialog();
            }
            else if (e.ColumnIndex == 5 && tipo == "A")
            {
                new DettagliDipendente(Convert.ToInt32(context.ORDINEs
                    .Where(o => o.idOrdine == idOrdine)
                    .Select(o => new { o.idDipendente })
                    .FirstOrDefault().idDipendente)).ShowDialog();
            }

        }
    }
}
