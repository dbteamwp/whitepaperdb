﻿namespace whitePaper
{
    partial class GestioneStampe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConfirmBt = new System.Windows.Forms.Button();
            this.idStampa = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.Label();
            this.Y = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tipoTb = new System.Windows.Forms.TextBox();
            this.tipoCartaTb = new System.Windows.Forms.TextBox();
            this.dimXTb = new System.Windows.Forms.TextBox();
            this.dimYTb = new System.Windows.Forms.TextBox();
            this.prezzoTb = new System.Windows.Forms.TextBox();
            this.scontoTb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.idL = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ConfirmBt
            // 
            this.ConfirmBt.Location = new System.Drawing.Point(110, 503);
            this.ConfirmBt.Name = "ConfirmBt";
            this.ConfirmBt.Size = new System.Drawing.Size(274, 58);
            this.ConfirmBt.TabIndex = 0;
            this.ConfirmBt.Text = "Conferma";
            this.ConfirmBt.UseVisualStyleBackColor = true;
            this.ConfirmBt.Click += new System.EventHandler(this.ConfirmBt_Click);
            // 
            // idStampa
            // 
            this.idStampa.AutoSize = true;
            this.idStampa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idStampa.Location = new System.Drawing.Point(29, 83);
            this.idStampa.Name = "idStampa";
            this.idStampa.Size = new System.Drawing.Size(272, 31);
            this.idStampa.TabIndex = 2;
            this.idStampa.Text = "Gestione stampa ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tipo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tipo carta";
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.Location = new System.Drawing.Point(3, 129);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(26, 25);
            this.X.TabIndex = 5;
            this.X.Text = "X";
            // 
            // Y
            // 
            this.Y.AutoSize = true;
            this.Y.Location = new System.Drawing.Point(3, 172);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(27, 25);
            this.Y.TabIndex = 6;
            this.Y.Text = "Y";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Prezzo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "Sconto";
            // 
            // tipoTb
            // 
            this.tipoTb.Location = new System.Drawing.Point(137, 3);
            this.tipoTb.MaxLength = 20;
            this.tipoTb.Name = "tipoTb";
            this.tipoTb.Size = new System.Drawing.Size(308, 31);
            this.tipoTb.TabIndex = 10;
            // 
            // tipoCartaTb
            // 
            this.tipoCartaTb.Location = new System.Drawing.Point(137, 46);
            this.tipoCartaTb.MaxLength = 20;
            this.tipoCartaTb.Name = "tipoCartaTb";
            this.tipoCartaTb.Size = new System.Drawing.Size(308, 31);
            this.tipoCartaTb.TabIndex = 11;
            // 
            // dimXTb
            // 
            this.dimXTb.Location = new System.Drawing.Point(137, 132);
            this.dimXTb.Name = "dimXTb";
            this.dimXTb.Size = new System.Drawing.Size(308, 31);
            this.dimXTb.TabIndex = 12;
            this.dimXTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // dimYTb
            // 
            this.dimYTb.Location = new System.Drawing.Point(137, 175);
            this.dimYTb.Name = "dimYTb";
            this.dimYTb.Size = new System.Drawing.Size(308, 31);
            this.dimYTb.TabIndex = 13;
            this.dimYTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // prezzoTb
            // 
            this.prezzoTb.Location = new System.Drawing.Point(137, 218);
            this.prezzoTb.Name = "prezzoTb";
            this.prezzoTb.Size = new System.Drawing.Size(308, 31);
            this.prezzoTb.TabIndex = 14;
            this.prezzoTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // scontoTb
            // 
            this.scontoTb.Location = new System.Drawing.Point(137, 261);
            this.scontoTb.Name = "scontoTb";
            this.scontoTb.Size = new System.Drawing.Size(308, 31);
            this.scontoTb.TabIndex = 15;
            this.scontoTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "DIMENSIONI";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tipoTb, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tipoCartaTb, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.scontoTb, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.dimXTb, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.X, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dimYTb, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.Y, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.prezzoTb, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(34, 161);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(448, 306);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // idL
            // 
            this.idL.AutoSize = true;
            this.idL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idL.Location = new System.Drawing.Point(321, 83);
            this.idL.Name = "idL";
            this.idL.Size = new System.Drawing.Size(93, 33);
            this.idL.TabIndex = 18;
            this.idL.Text = "NULL";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(498, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1411, 604);
            this.dataGridView1.TabIndex = 19;
            // 
            // GestioneStampe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1925, 627);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.idL);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ConfirmBt);
            this.Controls.Add(this.idStampa);
            this.Name = "GestioneStampe";
            this.Text = "Gestione listino stampe";
            this.Load += new System.EventHandler(this.GestioneStampe_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ConfirmBt;
        private System.Windows.Forms.Label idStampa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label X;
        private System.Windows.Forms.Label Y;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tipoTb;
        private System.Windows.Forms.TextBox tipoCartaTb;
        private System.Windows.Forms.TextBox dimXTb;
        private System.Windows.Forms.TextBox dimYTb;
        private System.Windows.Forms.TextBox prezzoTb;
        private System.Windows.Forms.TextBox scontoTb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label idL;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}