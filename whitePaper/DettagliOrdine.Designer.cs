﻿namespace whitePaper
{
    partial class DettagliOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.idL = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataRitiroTb = new System.Windows.Forms.TextBox();
            this.dataEvasioneTb = new System.Windows.Forms.TextBox();
            this.dataScadenzaEvasioneTb = new System.Windows.Forms.TextBox();
            this.dataInserimentoTb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridViewNote = new System.Windows.Forms.DataGridView();
            this.dataGridViewDettagliEStampe = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridViewPagamenti = new System.Windows.Forms.DataGridView();
            this.spedizioneL = new System.Windows.Forms.Label();
            this.costoL = new System.Windows.Forms.Label();
            this.corriereL = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.indirizzoSpedizioneL = new System.Windows.Forms.Label();
            this.importoTb = new System.Windows.Forms.TextBox();
            this.dataEmissioneTb = new System.Windows.Forms.TextBox();
            this.costoTb = new System.Windows.Forms.TextBox();
            this.indirizzoTb = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelSpedizione = new System.Windows.Forms.TableLayoutPanel();
            this.corriereBt = new System.Windows.Forms.Button();
            this.corriereTb = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.statoCb = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDettagliEStampe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPagamenti)).BeginInit();
            this.tableLayoutPanelSpedizione.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "ORDINE ID";
            // 
            // idL
            // 
            this.idL.AutoSize = true;
            this.idL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idL.Location = new System.Drawing.Point(233, 0);
            this.idL.Name = "idL";
            this.idL.Size = new System.Drawing.Size(104, 37);
            this.idL.TabIndex = 1;
            this.idL.Text = "NULL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data inserimento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "Data evasione";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 39);
            this.label4.TabIndex = 4;
            this.label4.Text = "Data scadenza evasione";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "Data ritiro";
            // 
            // dataRitiroTb
            // 
            this.dataRitiroTb.Location = new System.Drawing.Point(233, 159);
            this.dataRitiroTb.Name = "dataRitiroTb";
            this.dataRitiroTb.ReadOnly = true;
            this.dataRitiroTb.Size = new System.Drawing.Size(293, 31);
            this.dataRitiroTb.TabIndex = 48;
            // 
            // dataEvasioneTb
            // 
            this.dataEvasioneTb.Location = new System.Drawing.Point(233, 120);
            this.dataEvasioneTb.Name = "dataEvasioneTb";
            this.dataEvasioneTb.ReadOnly = true;
            this.dataEvasioneTb.Size = new System.Drawing.Size(293, 31);
            this.dataEvasioneTb.TabIndex = 47;
            // 
            // dataScadenzaEvasioneTb
            // 
            this.dataScadenzaEvasioneTb.Location = new System.Drawing.Point(233, 81);
            this.dataScadenzaEvasioneTb.Name = "dataScadenzaEvasioneTb";
            this.dataScadenzaEvasioneTb.ReadOnly = true;
            this.dataScadenzaEvasioneTb.Size = new System.Drawing.Size(293, 31);
            this.dataScadenzaEvasioneTb.TabIndex = 45;
            // 
            // dataInserimentoTb
            // 
            this.dataInserimentoTb.Location = new System.Drawing.Point(233, 42);
            this.dataInserimentoTb.Name = "dataInserimentoTb";
            this.dataInserimentoTb.ReadOnly = true;
            this.dataInserimentoTb.Size = new System.Drawing.Size(293, 31);
            this.dataInserimentoTb.TabIndex = 46;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 292);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 37);
            this.label8.TabIndex = 15;
            this.label8.Text = "NOTE";
            // 
            // dataGridViewNote
            // 
            this.dataGridViewNote.AllowUserToAddRows = false;
            this.dataGridViewNote.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewNote.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewNote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNote.Location = new System.Drawing.Point(29, 343);
            this.dataGridViewNote.Name = "dataGridViewNote";
            this.dataGridViewNote.ReadOnly = true;
            this.dataGridViewNote.RowTemplate.Height = 33;
            this.dataGridViewNote.Size = new System.Drawing.Size(768, 255);
            this.dataGridViewNote.TabIndex = 16;
            // 
            // dataGridViewDettagliEStampe
            // 
            this.dataGridViewDettagliEStampe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDettagliEStampe.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDettagliEStampe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDettagliEStampe.Location = new System.Drawing.Point(834, 506);
            this.dataGridViewDettagliEStampe.Name = "dataGridViewDettagliEStampe";
            this.dataGridViewDettagliEStampe.RowTemplate.Height = 33;
            this.dataGridViewDettagliEStampe.Size = new System.Drawing.Size(1207, 511);
            this.dataGridViewDettagliEStampe.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(827, 445);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(361, 37);
            this.label9.TabIndex = 27;
            this.label9.Text = "DETTAGLI E STAMPE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(827, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(215, 37);
            this.label11.TabIndex = 28;
            this.label11.Text = "PAGAMENTI";
            // 
            // dataGridViewPagamenti
            // 
            this.dataGridViewPagamenti.AllowUserToAddRows = false;
            this.dataGridViewPagamenti.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewPagamenti.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewPagamenti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPagamenti.Location = new System.Drawing.Point(834, 136);
            this.dataGridViewPagamenti.Name = "dataGridViewPagamenti";
            this.dataGridViewPagamenti.ReadOnly = true;
            this.dataGridViewPagamenti.RowTemplate.Height = 33;
            this.dataGridViewPagamenti.Size = new System.Drawing.Size(1207, 286);
            this.dataGridViewPagamenti.TabIndex = 29;
            // 
            // spedizioneL
            // 
            this.spedizioneL.AutoSize = true;
            this.spedizioneL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedizioneL.Location = new System.Drawing.Point(3, 0);
            this.spedizioneL.Name = "spedizioneL";
            this.spedizioneL.Size = new System.Drawing.Size(219, 37);
            this.spedizioneL.TabIndex = 30;
            this.spedizioneL.Text = "SPEDIZIONE";
            // 
            // costoL
            // 
            this.costoL.AutoSize = true;
            this.costoL.Location = new System.Drawing.Point(3, 83);
            this.costoL.Name = "costoL";
            this.costoL.Size = new System.Drawing.Size(68, 25);
            this.costoL.TabIndex = 31;
            this.costoL.Text = "Costo";
            // 
            // corriereL
            // 
            this.corriereL.AutoSize = true;
            this.corriereL.Location = new System.Drawing.Point(3, 166);
            this.corriereL.Name = "corriereL";
            this.corriereL.Size = new System.Drawing.Size(89, 25);
            this.corriereL.TabIndex = 32;
            this.corriereL.Text = "Corriere";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 25);
            this.label15.TabIndex = 33;
            this.label15.Text = "Importo fattura ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(304, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(160, 25);
            this.label16.TabIndex = 34;
            this.label16.Text = "Data emissione";
            // 
            // indirizzoSpedizioneL
            // 
            this.indirizzoSpedizioneL.AutoSize = true;
            this.indirizzoSpedizioneL.Location = new System.Drawing.Point(3, 249);
            this.indirizzoSpedizioneL.Name = "indirizzoSpedizioneL";
            this.indirizzoSpedizioneL.Size = new System.Drawing.Size(202, 25);
            this.indirizzoSpedizioneL.TabIndex = 36;
            this.indirizzoSpedizioneL.Text = "Indirizzo spedizione";
            // 
            // importoTb
            // 
            this.importoTb.Location = new System.Drawing.Point(3, 30);
            this.importoTb.Name = "importoTb";
            this.importoTb.ReadOnly = true;
            this.importoTb.Size = new System.Drawing.Size(295, 31);
            this.importoTb.TabIndex = 37;
            // 
            // dataEmissioneTb
            // 
            this.dataEmissioneTb.Location = new System.Drawing.Point(304, 30);
            this.dataEmissioneTb.Name = "dataEmissioneTb";
            this.dataEmissioneTb.ReadOnly = true;
            this.dataEmissioneTb.Size = new System.Drawing.Size(295, 31);
            this.dataEmissioneTb.TabIndex = 38;
            // 
            // costoTb
            // 
            this.costoTb.Location = new System.Drawing.Point(257, 86);
            this.costoTb.Name = "costoTb";
            this.costoTb.ReadOnly = true;
            this.costoTb.Size = new System.Drawing.Size(296, 31);
            this.costoTb.TabIndex = 39;
            // 
            // indirizzoTb
            // 
            this.indirizzoTb.Location = new System.Drawing.Point(257, 252);
            this.indirizzoTb.Multiline = true;
            this.indirizzoTb.Name = "indirizzoTb";
            this.indirizzoTb.ReadOnly = true;
            this.indirizzoTb.Size = new System.Drawing.Size(293, 54);
            this.indirizzoTb.TabIndex = 42;
            // 
            // tableLayoutPanelSpedizione
            // 
            this.tableLayoutPanelSpedizione.ColumnCount = 2;
            this.tableLayoutPanelSpedizione.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.64116F));
            this.tableLayoutPanelSpedizione.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.35884F));
            this.tableLayoutPanelSpedizione.Controls.Add(this.spedizioneL, 0, 0);
            this.tableLayoutPanelSpedizione.Controls.Add(this.indirizzoTb, 1, 3);
            this.tableLayoutPanelSpedizione.Controls.Add(this.costoTb, 1, 1);
            this.tableLayoutPanelSpedizione.Controls.Add(this.costoL, 0, 1);
            this.tableLayoutPanelSpedizione.Controls.Add(this.indirizzoSpedizioneL, 0, 3);
            this.tableLayoutPanelSpedizione.Controls.Add(this.corriereL, 0, 2);
            this.tableLayoutPanelSpedizione.Controls.Add(this.corriereBt, 1, 0);
            this.tableLayoutPanelSpedizione.Controls.Add(this.corriereTb, 1, 2);
            this.tableLayoutPanelSpedizione.Location = new System.Drawing.Point(29, 635);
            this.tableLayoutPanelSpedizione.Name = "tableLayoutPanelSpedizione";
            this.tableLayoutPanelSpedizione.RowCount = 4;
            this.tableLayoutPanelSpedizione.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelSpedizione.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelSpedizione.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelSpedizione.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelSpedizione.Size = new System.Drawing.Size(758, 335);
            this.tableLayoutPanelSpedizione.TabIndex = 43;
            // 
            // corriereBt
            // 
            this.corriereBt.Location = new System.Drawing.Point(257, 3);
            this.corriereBt.Name = "corriereBt";
            this.corriereBt.Size = new System.Drawing.Size(296, 46);
            this.corriereBt.TabIndex = 44;
            this.corriereBt.Text = "modifica corriere";
            this.corriereBt.UseVisualStyleBackColor = true;
            this.corriereBt.Click += new System.EventHandler(this.corriereBt_Click);
            // 
            // corriereTb
            // 
            this.corriereTb.Location = new System.Drawing.Point(257, 169);
            this.corriereTb.Name = "corriereTb";
            this.corriereTb.ReadOnly = true;
            this.corriereTb.Size = new System.Drawing.Size(296, 31);
            this.corriereTb.TabIndex = 45;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.importoTb, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label16, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.dataEmissioneTb, 1, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(1439, 31);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(602, 93);
            this.tableLayoutPanel4.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 25);
            this.label6.TabIndex = 6;
            this.label6.Text = "Stato";
            // 
            // statoCb
            // 
            this.statoCb.FormattingEnabled = true;
            this.statoCb.Items.AddRange(new object[] {
            "IN ATTESA",
            "IN LAVORAZIONE",
            "SPEDITO",
            "CONSEGNATO"});
            this.statoCb.Location = new System.Drawing.Point(233, 198);
            this.statoCb.Name = "statoCb";
            this.statoCb.Size = new System.Drawing.Size(293, 33);
            this.statoCb.TabIndex = 45;
            this.statoCb.SelectedIndexChanged += new System.EventHandler(this.statoCb_SelectedIndexChanged);
            this.statoCb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_KeyPress);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataRitiroTb, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.dataEvasioneTb, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dataScadenzaEvasioneTb, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.statoCb, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.idL, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataInserimentoTb, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(29, 31);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(768, 240);
            this.tableLayoutPanel1.TabIndex = 49;
            // 
            // DettagliOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2053, 1049);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanelSpedizione);
            this.Controls.Add(this.dataGridViewPagamenti);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dataGridViewDettagliEStampe);
            this.Controls.Add(this.dataGridViewNote);
            this.Controls.Add(this.label8);
            this.Name = "DettagliOrdine";
            this.Text = "Dettagli ordine";
            this.Load += new System.EventHandler(this.DettagliOrdine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDettagliEStampe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPagamenti)).EndInit();
            this.tableLayoutPanelSpedizione.ResumeLayout(false);
            this.tableLayoutPanelSpedizione.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label idL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridViewNote;
        private System.Windows.Forms.DataGridView dataGridViewDettagliEStampe;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridViewPagamenti;
        private System.Windows.Forms.Label spedizioneL;
        private System.Windows.Forms.Label costoL;
        private System.Windows.Forms.Label corriereL;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label indirizzoSpedizioneL;
        private System.Windows.Forms.TextBox importoTb;
        private System.Windows.Forms.TextBox dataEmissioneTb;
        private System.Windows.Forms.TextBox costoTb;
        private System.Windows.Forms.TextBox indirizzoTb;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelSpedizione;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox dataRitiroTb;
        private System.Windows.Forms.TextBox dataEvasioneTb;
        private System.Windows.Forms.TextBox dataScadenzaEvasioneTb;
        private System.Windows.Forms.TextBox dataInserimentoTb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox statoCb;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button corriereBt;
        private System.Windows.Forms.TextBox corriereTb;
    }
}