﻿namespace whitePaper
{
    partial class query
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ClientiBt2 = new System.Windows.Forms.Button();
            this.topDipendentiBt = new System.Windows.Forms.Button();
            this.ClientiBt = new System.Windows.Forms.Button();
            this.StampeBt = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 98);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1360, 374);
            this.dataGridView1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1366, 475);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.ClientiBt2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.topDipendentiBt, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ClientiBt, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.StampeBt, 3, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1360, 89);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // ClientiBt2
            // 
            this.ClientiBt2.Location = new System.Drawing.Point(683, 3);
            this.ClientiBt2.Name = "ClientiBt2";
            this.ClientiBt2.Size = new System.Drawing.Size(247, 83);
            this.ClientiBt2.TabIndex = 2;
            this.ClientiBt2.Text = "Classifica TOP 10 Clienti - Spese";
            this.ClientiBt2.UseVisualStyleBackColor = true;
            this.ClientiBt2.Click += new System.EventHandler(this.ClientiBt2_Click);
            // 
            // topDipendentiBt
            // 
            this.topDipendentiBt.Location = new System.Drawing.Point(3, 3);
            this.topDipendentiBt.Name = "topDipendentiBt";
            this.topDipendentiBt.Size = new System.Drawing.Size(247, 83);
            this.topDipendentiBt.TabIndex = 0;
            this.topDipendentiBt.Text = "Classifica TOP 3 DIPENDENTI";
            this.topDipendentiBt.UseVisualStyleBackColor = true;
            this.topDipendentiBt.Click += new System.EventHandler(this.TopDipendentiBt_Click);
            // 
            // ClientiBt
            // 
            this.ClientiBt.Location = new System.Drawing.Point(343, 3);
            this.ClientiBt.Name = "ClientiBt";
            this.ClientiBt.Size = new System.Drawing.Size(247, 83);
            this.ClientiBt.TabIndex = 1;
            this.ClientiBt.Text = "Classifica TOP 10 Clienti - Spese";
            this.ClientiBt.UseVisualStyleBackColor = true;
            this.ClientiBt.Click += new System.EventHandler(this.ClientiBt_Click);
            // 
            // StampeBt
            // 
            this.StampeBt.Location = new System.Drawing.Point(1023, 3);
            this.StampeBt.Name = "StampeBt";
            this.StampeBt.Size = new System.Drawing.Size(247, 83);
            this.StampeBt.TabIndex = 3;
            this.StampeBt.Text = "TOP 10 Stampe ";
            this.StampeBt.UseVisualStyleBackColor = true;
            this.StampeBt.Click += new System.EventHandler(this.StampeBt_Click);
            // 
            // query
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1366, 475);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "query";
            this.Text = "Interrogazioni";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button topDipendentiBt;
        private System.Windows.Forms.Button ClientiBt;
        private System.Windows.Forms.Button ClientiBt2;
        private System.Windows.Forms.Button StampeBt;
    }
}