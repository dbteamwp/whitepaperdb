﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class EffettuaPagamento : Form
    {
        private int idPagamento;
        private int idFattura;
        public EffettuaPagamento(int idPagamento, int idFattura)
        {
            InitializeComponent();
            this.idPagamento = idPagamento;
            this.idFattura = idFattura;
        }

        private void ConfirmBt_Click(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            var queryPagamento =
                from p in context.PAGAMENTOs
                where p.idFattura == this.idFattura
                && p.codicePagamento == this.idPagamento
                select p;
            foreach (var pag in queryPagamento)
            {
                pag.dataPagamento = DateTime.Now;
                pag.tipoPagamento = context.TIPO_PAGAMENTOs.Where(p => p.nome == tipoPagamentoLb.SelectedItem.ToString()).First().nome;
                pag.TIPO_PAGAMENTO = context.TIPO_PAGAMENTOs.Where(p => p.nome == tipoPagamentoLb.SelectedItem.ToString()).First();
            }
            context.SubmitChanges();
            this.Dispose();
        }

        private void EffettuaPagamento_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.tipoPagamentoLb.DataSource = context.TIPO_PAGAMENTOs.Select(p => p.nome);
        }
    }
}
