﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class GestioneStampe : Form
    {
        public GestioneStampe()
        {
            InitializeComponent();
        }


        private void GestioneStampe_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.idL.Text = NewIdStampa(context).ToString();
            this.Sync_DataGridView(context);
            DataGridViewButtonColumn modbut = new DataGridViewButtonColumn();
            modbut.Name = "dataGridViewModifyButton";
            modbut.Text = "modifica";
            modbut.HeaderText = "modifica";
            modbut.Width = 20;
            modbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            modbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(modbut);
            DataGridViewButtonColumn delbut = new DataGridViewButtonColumn();
            delbut.Name = "dataGridViewDeleteButton";
            delbut.Text = "elimina";
            delbut.HeaderText = "elimina";
            delbut.Width = 20;
            delbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            delbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(delbut);
            dataGridView1.CellClick += DataGridView1_CellClick;
        }

        private void ConfirmBt_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tipoCartaTb.Text) || String.IsNullOrWhiteSpace(tipoTb.Text)
                || String.IsNullOrWhiteSpace(prezzoTb.Text)
                || String.IsNullOrWhiteSpace(dimXTb.Text)
                || String.IsNullOrWhiteSpace(dimYTb.Text)
                || float.Parse(dimXTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) <= 0
                || float.Parse(dimYTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) <= 0
                || float.Parse(prezzoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) <= 0)
            {
                MessageBox.Show("Dati incompleti");
                return;
            }

            if (!String.IsNullOrWhiteSpace(scontoTb.Text) &&
                (float.Parse(scontoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) <= 0
                || float.Parse(scontoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat) >= 100))
            {
                MessageBox.Show("Dati incompleti");
                return;
            }
                
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            if (Convert.ToInt32(idL.Text) < NewIdStampa(context))
            {
                //modifica
                var stampeQuery =
                   from s in context.STAMPAs
                   where s.ID == Convert.ToInt32(idL.Text)
                   select s;

                foreach (var stampa in stampeQuery)
                {
                    stampa.tipo = tipoTb.Text;
                    stampa.tipoCarta = tipoCartaTb.Text;
                    stampa.ID = Convert.ToInt32(idL.Text);
                    stampa.prezzo = float.Parse(prezzoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if (!String.IsNullOrWhiteSpace(scontoTb.Text))
                    {
                        stampa.sconto = float.Parse(scontoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    }
                    stampa.dimX = float.Parse(dimXTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    stampa.dimY = float.Parse(dimYTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                }
            }
            else
            {
                //inserisci
                STAMPA stampa = new STAMPA();
                stampa.tipo = tipoTb.Text;
                stampa.tipoCarta = tipoCartaTb.Text;
                stampa.ID = Convert.ToInt32(idL.Text);
                stampa.prezzo = float.Parse(prezzoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                if (!String.IsNullOrWhiteSpace(scontoTb.Text))
                {
                    stampa.sconto = float.Parse(scontoTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                }
                stampa.dimX = float.Parse(dimXTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                stampa.dimY = float.Parse(dimYTb.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                context.STAMPAs.InsertOnSubmit(stampa);
            }
            context.SubmitChanges();
            this.Dispose();
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 7 && e.ColumnIndex != 8) { return; }
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex == 8)
            {
                if (MessageBox.Show("Sei sicuro di eliminare la stampa " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                int idStampa = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                if(context.DETTAGLIO_ORDINEs.Where(d => d.idStampa == idStampa).Any())
                {
                    MessageBox.Show("Non è possibile eliminare questa stampa, poichè è presente in alcuni ordini!!");
                    return;
                }
                var listRes = from p in context.STAMPAs
                              where p.ID == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value)
                              select p;
                if (listRes.Count() > 0)
                {
                    context.STAMPAs.DeleteOnSubmit(listRes.First());
                    context.SubmitChanges();
                }
                dataGridView1.Rows.RemoveAt(e.RowIndex);

            }
            else if (e.ColumnIndex == 7)
            {
                idL.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                //aggiornare i campi con questi dati
                tipoTb.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                tipoCartaTb.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                dimXTb.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                dimYTb.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
                prezzoTb.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
                if(dataGridView1.Rows[e.RowIndex].Cells[6].Value != null)
                {
                    scontoTb.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
                }
                
            }
        }

        private static int NewIdStampa(WhitePaperDataClassesDataContext context)
        {
            return (context.STAMPAs.Select(x => (int?)x.ID).Max() ?? 0) + 1;
        }

        private void Sync_DataGridView(WhitePaperDataClassesDataContext context)
        {
            dataGridView1.DataSource = context.STAMPAs.GetNewBindingList();
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
