﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class DettagliCliente : Form
    {
        private int idCliente;

        public DettagliCliente(int idCliente)
        {
            this.idCliente = idCliente;
            InitializeComponent();
        }

        private void DettagliCliente_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            CLIENTE cliente = context.CLIENTEs.Where(c => c.idCliente == this.idCliente).First();
            this.idClienteL.Text = cliente.idCliente.ToString();
            this.nominativoTb.Text = cliente.ragioneSociale;
            this.cf_pivaTb.Text = cliente.CF_PIVA;
            this.mailTb.Text = cliente.mail;
            if(cliente.Tipo_azienda == '1')
            {
                this.nominativoL.Text = "Ragione sociale";
                this.cf_pivaL.Text = "PIVA";
            } else
            {
                this.cf_pivaL.Text = "CF";
            }

        }
    }
}
