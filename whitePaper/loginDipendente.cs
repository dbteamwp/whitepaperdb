﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class loginDipendente : Form
    {
        public loginDipendente()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(idDipendente.Text) || String.IsNullOrWhiteSpace(password.Text))
            {
                MessageBox.Show("Compilare tutti i campi obbligatori");
                return;
            }
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            var q = from p in context.DIPENDENTEs
                    where p.idDipendente == Convert.ToInt32(idDipendente.Text)
                    && p.password == password.Text
                    select p;

            if (!q.Any())
            {
                MessageBox.Show("Dati login errati", "WhitePaper");
            } else
            {
                //LOGIN EFFETTUATO
                this.Hide();
                new homeDipendente(q.First()).ShowDialog();
                this.Close();

            }
        }

        private void IdDipendente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
