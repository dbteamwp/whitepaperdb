﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class FatturatoAnnuo : Form
    {
        public FatturatoAnnuo()
        {
            InitializeComponent();
        }

        private void annoUd_ValueChanged(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            try
            {
                importoTb.Text = context.PAGAMENTOs
                .Where(p => p.dataPagamento != null
                && p.dataPagamento.Value.Year == annoUd.Value)
                .Sum(p => p.importo)
                .ToString() + "€";
            }catch 
            {
                importoTb.Text = null;
            }
        }
    }
}
