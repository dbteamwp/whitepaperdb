﻿using System;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class homeDipendente : Form
    {
        private DIPENDENTE dipendente;
        public homeDipendente(DIPENDENTE d)
        {
            this.dipendente = d;
            InitializeComponent();
            if(d.capo != '1')
            {
                buttonCapo.Hide();
            }
        }

        private void Capo_Click(object sender, EventArgs e)
        {
            new formCapo(dipendente.idDipendente).ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            new visualizzaOrdini(dipendente.idDipendente, "D").ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new visualizzaOrdini(dipendente.idDipendente, "C").ShowDialog();
        }

        private void DatiPersonaliBt_Click(object sender, EventArgs e)
        {
            new DatiDipendente(dipendente.idDipendente).ShowDialog();
        }

        private void PagamentiBt_Click(object sender, EventArgs e)
        {
            new PagamentiForm(dipendente.idDipendente, "D").ShowDialog();
        }

        private void scadenzaOrdiniBt_Click(object sender, EventArgs e)
        {
            new visualizzaOrdini(dipendente.idDipendente, "S").ShowDialog();
        }
    }
}
