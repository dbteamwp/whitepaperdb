﻿namespace whitePaper
{
    partial class DettagliDipendente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.nomeTb = new System.Windows.Forms.TextBox();
            this.cognomeTb = new System.Windows.Forms.TextBox();
            this.nomeL = new System.Windows.Forms.Label();
            this.cfTb = new System.Windows.Forms.TextBox();
            this.cf_pivaL = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.idDipendenteL = new System.Windows.Forms.Label();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.nomeTb, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cognomeTb, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.nomeL, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cfTb, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.cf_pivaL, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.idDipendenteL, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(765, 225);
            this.tableLayoutPanel2.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "CF";
            // 
            // nomeTb
            // 
            this.nomeTb.Location = new System.Drawing.Point(385, 59);
            this.nomeTb.Name = "nomeTb";
            this.nomeTb.ReadOnly = true;
            this.nomeTb.Size = new System.Drawing.Size(302, 31);
            this.nomeTb.TabIndex = 22;
            // 
            // cognomeTb
            // 
            this.cognomeTb.Location = new System.Drawing.Point(385, 115);
            this.cognomeTb.Name = "cognomeTb";
            this.cognomeTb.ReadOnly = true;
            this.cognomeTb.Size = new System.Drawing.Size(302, 31);
            this.cognomeTb.TabIndex = 23;
            // 
            // nomeL
            // 
            this.nomeL.AutoSize = true;
            this.nomeL.Location = new System.Drawing.Point(3, 56);
            this.nomeL.Name = "nomeL";
            this.nomeL.Size = new System.Drawing.Size(68, 25);
            this.nomeL.TabIndex = 19;
            this.nomeL.Text = "Nome";
            // 
            // cfTb
            // 
            this.cfTb.Location = new System.Drawing.Point(385, 171);
            this.cfTb.Name = "cfTb";
            this.cfTb.ReadOnly = true;
            this.cfTb.Size = new System.Drawing.Size(302, 31);
            this.cfTb.TabIndex = 24;
            // 
            // cf_pivaL
            // 
            this.cf_pivaL.AutoSize = true;
            this.cf_pivaL.Location = new System.Drawing.Point(3, 112);
            this.cf_pivaL.Name = "cf_pivaL";
            this.cf_pivaL.Size = new System.Drawing.Size(104, 25);
            this.cf_pivaL.TabIndex = 21;
            this.cf_pivaL.Text = "Cognome";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(276, 37);
            this.label7.TabIndex = 7;
            this.label7.Text = "DIPENDENTE ID";
            // 
            // idDipendenteL
            // 
            this.idDipendenteL.AutoSize = true;
            this.idDipendenteL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idDipendenteL.Location = new System.Drawing.Point(385, 0);
            this.idDipendenteL.Name = "idDipendenteL";
            this.idDipendenteL.Size = new System.Drawing.Size(104, 37);
            this.idDipendenteL.TabIndex = 8;
            this.idDipendenteL.Text = "NULL";
            // 
            // DettagliDipendente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 245);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "DettagliDipendente";
            this.Text = "Dettagli dipendente";
            this.Load += new System.EventHandler(this.DettagliDipendente_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox nomeTb;
        private System.Windows.Forms.TextBox cognomeTb;
        private System.Windows.Forms.Label nomeL;
        private System.Windows.Forms.Label cf_pivaL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label idDipendenteL;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox cfTb;
    }
}