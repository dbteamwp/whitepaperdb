﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class gestioneTipoPagamenti : Form
    {
        public gestioneTipoPagamenti()
        {
            InitializeComponent();
        }

        private void gestioneTipoPagamenti_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.Sync_DataGridView(context);
            DataGridViewButtonColumn delbut = new DataGridViewButtonColumn();
            delbut.Name = "dataGridViewDeleteButton";
            delbut.Text = "elimina";
            delbut.HeaderText = "elimina";
            delbut.Width = 20;
            delbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            delbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(delbut);
            dataGridView1.CellClick += DataGridView1_CellClick;
        }

        private void confermaBt_Click(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            nomeTb.Text = nomeTb.Text.ToUpper();
            if (!String.IsNullOrEmpty(nomeTb.Text) && !context.TIPO_PAGAMENTOs.Where(p => p.nome == nomeTb.Text).Any())
            {
                TIPO_PAGAMENTO p = new TIPO_PAGAMENTO();
                p.nome = this.nomeTb.Text;
                context.TIPO_PAGAMENTOs.InsertOnSubmit(p);
                context.SubmitChanges();
                this.Dispose();
            } 
            else
            {
                MessageBox.Show("Dati non corretti");
            }
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 1) { return; }
            if (e.RowIndex < 0) return;
            if (MessageBox.Show("Sei sicuro di eliminare il tipo di pagamento " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            var listRes = from p in context.TIPO_PAGAMENTOs
                            where p.nome.Equals(dataGridView1.Rows[e.RowIndex].Cells[0].Value)
                            select p;
            if (listRes.Count() > 0)
            {
                context.TIPO_PAGAMENTOs.DeleteOnSubmit(listRes.First());
                context.SubmitChanges();
            }
            dataGridView1.Rows.RemoveAt(e.RowIndex);
        }

        private void Sync_DataGridView(WhitePaperDataClassesDataContext context)
        {
            dataGridView1.DataSource = context.TIPO_PAGAMENTOs.GetNewBindingList();
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }
    }
}
