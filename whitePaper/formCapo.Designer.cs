﻿namespace whitePaper
{
    partial class formCapo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDipendenti = new System.Windows.Forms.Button();
            this.listinoBt = new System.Windows.Forms.Button();
            this.OrdiniBt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.corrieriBt = new System.Windows.Forms.Button();
            this.ClientiBt = new System.Windows.Forms.Button();
            this.PagamentiBt = new System.Windows.Forms.Button();
            this.scadenzeBt = new System.Windows.Forms.Button();
            this.queryBt = new System.Windows.Forms.Button();
            this.fatturatoBt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonDipendenti
            // 
            this.buttonDipendenti.Location = new System.Drawing.Point(3, 3);
            this.buttonDipendenti.Name = "buttonDipendenti";
            this.buttonDipendenti.Size = new System.Drawing.Size(183, 74);
            this.buttonDipendenti.TabIndex = 0;
            this.buttonDipendenti.Text = "Gestione dipendenti";
            this.buttonDipendenti.UseVisualStyleBackColor = true;
            this.buttonDipendenti.Click += new System.EventHandler(this.buttonDipendenti_Click);
            // 
            // listinoBt
            // 
            this.listinoBt.Location = new System.Drawing.Point(192, 3);
            this.listinoBt.Name = "listinoBt";
            this.listinoBt.Size = new System.Drawing.Size(183, 74);
            this.listinoBt.TabIndex = 1;
            this.listinoBt.Text = "Gestione listino prezzi";
            this.listinoBt.UseVisualStyleBackColor = true;
            this.listinoBt.Click += new System.EventHandler(this.ListinoBt_Click);
            // 
            // OrdiniBt
            // 
            this.OrdiniBt.Location = new System.Drawing.Point(3, 97);
            this.OrdiniBt.Name = "OrdiniBt";
            this.OrdiniBt.Size = new System.Drawing.Size(183, 73);
            this.OrdiniBt.TabIndex = 4;
            this.OrdiniBt.Text = "Tutti gli ordini";
            this.OrdiniBt.UseVisualStyleBackColor = true;
            this.OrdiniBt.Click += new System.EventHandler(this.OrdiniBt_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.OrdiniBt, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonDipendenti, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.listinoBt, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.corrieriBt, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ClientiBt, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.PagamentiBt, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.scadenzeBt, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.queryBt, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.fatturatoBt, 2, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(61, 37);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(569, 283);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // corrieriBt
            // 
            this.corrieriBt.Location = new System.Drawing.Point(192, 97);
            this.corrieriBt.Name = "corrieriBt";
            this.corrieriBt.Size = new System.Drawing.Size(183, 73);
            this.corrieriBt.TabIndex = 5;
            this.corrieriBt.Text = "Gestione corrieri";
            this.corrieriBt.UseVisualStyleBackColor = true;
            this.corrieriBt.Click += new System.EventHandler(this.corrieriBt_Click);
            // 
            // ClientiBt
            // 
            this.ClientiBt.Location = new System.Drawing.Point(3, 191);
            this.ClientiBt.Name = "ClientiBt";
            this.ClientiBt.Size = new System.Drawing.Size(183, 73);
            this.ClientiBt.TabIndex = 6;
            this.ClientiBt.Text = "Gestione clienti";
            this.ClientiBt.UseVisualStyleBackColor = true;
            this.ClientiBt.Click += new System.EventHandler(this.ClientiBt_Click);
            // 
            // PagamentiBt
            // 
            this.PagamentiBt.Location = new System.Drawing.Point(192, 191);
            this.PagamentiBt.Name = "PagamentiBt";
            this.PagamentiBt.Size = new System.Drawing.Size(183, 73);
            this.PagamentiBt.TabIndex = 7;
            this.PagamentiBt.Text = "Gestione tipo pagamenti";
            this.PagamentiBt.UseVisualStyleBackColor = true;
            this.PagamentiBt.Click += new System.EventHandler(this.PagamentiBt_Click);
            // 
            // scadenzeBt
            // 
            this.scadenzeBt.Location = new System.Drawing.Point(381, 3);
            this.scadenzeBt.Name = "scadenzeBt";
            this.scadenzeBt.Size = new System.Drawing.Size(183, 74);
            this.scadenzeBt.TabIndex = 8;
            this.scadenzeBt.Text = "Pagamenti in scadenza";
            this.scadenzeBt.UseVisualStyleBackColor = true;
            this.scadenzeBt.Click += new System.EventHandler(this.scadenzeBt_Click);
            // 
            // queryBt
            // 
            this.queryBt.Location = new System.Drawing.Point(381, 97);
            this.queryBt.Name = "queryBt";
            this.queryBt.Size = new System.Drawing.Size(183, 73);
            this.queryBt.TabIndex = 9;
            this.queryBt.Text = "Interrogazioni";
            this.queryBt.UseVisualStyleBackColor = true;
            this.queryBt.Click += new System.EventHandler(this.queryBt_Click);
            // 
            // fatturatoBt
            // 
            this.fatturatoBt.Location = new System.Drawing.Point(381, 191);
            this.fatturatoBt.Name = "fatturatoBt";
            this.fatturatoBt.Size = new System.Drawing.Size(183, 73);
            this.fatturatoBt.TabIndex = 10;
            this.fatturatoBt.Text = "Fatturato annuo";
            this.fatturatoBt.UseVisualStyleBackColor = true;
            this.fatturatoBt.Click += new System.EventHandler(this.fatturatoBt_Click);
            // 
            // formCapo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 352);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "formCapo";
            this.Text = "HOME | Admin";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDipendenti;
        private System.Windows.Forms.Button listinoBt;
        private System.Windows.Forms.Button OrdiniBt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button corrieriBt;
        private System.Windows.Forms.Button ClientiBt;
        private System.Windows.Forms.Button PagamentiBt;
        private System.Windows.Forms.Button scadenzeBt;
        private System.Windows.Forms.Button queryBt;
        private System.Windows.Forms.Button fatturatoBt;
    }
}