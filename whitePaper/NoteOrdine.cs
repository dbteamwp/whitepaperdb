﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class NoteOrdine : Form
    {
        private int idOrdine;
        public NoteOrdine(int idOrdine)
        {
            InitializeComponent();
            this.idOrdine = idOrdine;
        }

        private void NotaBt_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrWhiteSpace(this.notaTb.Text))
            {
                MessageBox.Show("Dati errati!");
                return;
            }
            NOTA_ORDINE nota = new NOTA_ORDINE();
            nota.testoNota = this.notaTb.Text;
            nota.dataOraInserimento = DateTime.Now;
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            nota.ORDINE = context.ORDINEs.Where(o => o.idOrdine == this.idOrdine).First();
            context.NOTA_ORDINEs.InsertOnSubmit(nota);
            context.SubmitChanges();
            aggiornaDataGridView(context);
            notaTb.Clear();
        }

        private void NoteOrdine_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            aggiornaDataGridView(context);
        }

        private void aggiornaDataGridView(WhitePaperDataClassesDataContext context)
        {
            this.dataGridViewNote.DataSource = context.NOTA_ORDINEs.Where(n => n.idOrdine == this.idOrdine)
                    .Select(n => new { n.dataOraInserimento, n.testoNota }).OrderByDescending(o => o.dataOraInserimento);
        }
    }
}
