﻿namespace whitePaper
{
    partial class LoginCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mailBox = new System.Windows.Forms.TextBox();
            this.pwBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accediButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.registratiLabel = new System.Windows.Forms.LinkLabel();
            this.datiErratiLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mailBox
            // 
            this.mailBox.Location = new System.Drawing.Point(209, 81);
            this.mailBox.MaxLength = 25;
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(357, 31);
            this.mailBox.TabIndex = 0;
            // 
            // pwBox
            // 
            this.pwBox.Location = new System.Drawing.Point(209, 152);
            this.pwBox.MaxLength = 20;
            this.pwBox.Name = "pwBox";
            this.pwBox.Size = new System.Drawing.Size(357, 31);
            this.pwBox.TabIndex = 1;
            this.pwBox.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(91, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password:";
            // 
            // accediButton
            // 
            this.accediButton.Location = new System.Drawing.Point(261, 231);
            this.accediButton.Name = "accediButton";
            this.accediButton.Size = new System.Drawing.Size(199, 83);
            this.accediButton.TabIndex = 4;
            this.accediButton.Text = "Accedi";
            this.accediButton.UseVisualStyleBackColor = true;
            this.accediButton.Click += new System.EventHandler(this.accediButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 355);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Non sei registrato?";
            // 
            // registratiLabel
            // 
            this.registratiLabel.AutoSize = true;
            this.registratiLabel.Location = new System.Drawing.Point(403, 355);
            this.registratiLabel.Name = "registratiLabel";
            this.registratiLabel.Size = new System.Drawing.Size(109, 25);
            this.registratiLabel.TabIndex = 6;
            this.registratiLabel.TabStop = true;
            this.registratiLabel.Text = "Registrati!";
            this.registratiLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.registratiLabel_LinkClicked);
            // 
            // datiErratiLabel
            // 
            this.datiErratiLabel.AutoSize = true;
            this.datiErratiLabel.ForeColor = System.Drawing.Color.Crimson;
            this.datiErratiLabel.Location = new System.Drawing.Point(256, 30);
            this.datiErratiLabel.Name = "datiErratiLabel";
            this.datiErratiLabel.Size = new System.Drawing.Size(229, 25);
            this.datiErratiLabel.TabIndex = 7;
            this.datiErratiLabel.Text = "Mail o password errati!";
            this.datiErratiLabel.Visible = false;
            // 
            // LoginCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 430);
            this.Controls.Add(this.datiErratiLabel);
            this.Controls.Add(this.registratiLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.accediButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pwBox);
            this.Controls.Add(this.mailBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LoginCliente";
            this.Text = "WhitePaper | Login Cliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox mailBox;
        private System.Windows.Forms.TextBox pwBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button accediButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel registratiLabel;
        private System.Windows.Forms.Label datiErratiLabel;
    }
}