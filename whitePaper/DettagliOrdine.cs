﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class DettagliOrdine : Form
    {
        private int idOrdine;
        private ORDINE ordine;
        private FATTURA fattura;
        private String tipo;
        public DettagliOrdine(int idOrdine, String tipo)
        {
            this.idOrdine = idOrdine;
            this.tipo = tipo;
            InitializeComponent();
        }

        private void DettagliOrdine_Load(object sender, EventArgs e)
        {
            this.idL.Text = this.idOrdine.ToString();
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.ordine = context.ORDINEs.Where(o => o.idOrdine == this.idOrdine).First();
            this.dataInserimentoTb.Text = ordine.dataInserimento.ToString("dd/MM/yyyy");
            this.dataScadenzaEvasioneTb.Text = ordine.dataScadenzaEvasione?.ToString("dd/MM/yyyy");
            this.dataRitiroTb.Text = ordine.dataRitiro?.ToString("dd/MM/yyyy");
            this.dataEvasioneTb.Text = ordine.dataEvasione?.ToString("dd/MM/yyyy");
            statoCb.Text = ordine.stato.TrimEnd();
            this.dataGridViewNote.DataSource = context.NOTA_ORDINEs.Where(n => n.idOrdine == this.idOrdine)
                .Select(n => new { n.dataOraInserimento, n.testoNota }).OrderByDescending(o => o.dataOraInserimento);
            this.dataGridViewNote.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            this.fattura = context.FATTURAs.Where(f => f.idOrdine == this.idOrdine).First();
            this.dataGridViewPagamenti.DataSource = context.PAGAMENTOs.Where(p => p.idFattura == fattura.idFattura)
                .Select(p => new { p.codicePagamento, p.importo, p.dataPagamento, p.dataScadenza, p.tipoPagamento });
            //pulsante pagato
            if (tipo != "C")
            {
                DataGridViewButtonColumn paga = new DataGridViewButtonColumn();
                paga.Name = "dataGridViewPagaButton";
                paga.Text = "Pagato";
                paga.HeaderText = "Pagato";
                paga.Width = 20;
                paga.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                paga.UseColumnTextForButtonValue = true;
                dataGridViewPagamenti.Columns.Add(paga);
                dataGridViewPagamenti.CellClick += DataGridPagamento_CellClick;
            }
            this.dataGridViewPagamenti.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.importoTb.Text = fattura.importo.ToString() + "€";
            this.dataEmissioneTb.Text = fattura.dataEmissione?.ToString("dd/MM/yyyy");
            if (context.SPEDIZIONEs.Where(s => s.idOrdine == this.idOrdine).Any())
            {
                SPEDIZIONE spedizione = context.SPEDIZIONEs.Where(s => s.idOrdine == this.idOrdine).First();
                this.costoTb.Text = spedizione.costoSpedizione.ToString();
                this.indirizzoTb.Text = spedizione.INDIRIZZO.città.TrimEnd() + " - " + spedizione.INDIRIZZO.via.TrimEnd() + ", " + spedizione.INDIRIZZO.numeroCivico.TrimEnd();
                if (ordine.stato.TrimEnd() == "SPEDITO" || ordine.stato.TrimEnd() == "CONSEGNATO")
                {
                    this.corriereBt.Enabled = false;
                }

                if (context.CORRIEREs.Where(c => c.idCorriere == spedizione.idCorriere).Any())
                {
                    this.corriereTb.Text = context.CORRIEREs.Where(c => c.idCorriere == spedizione.idCorriere).First().ToString();
                }

            }
            else
            {
                statoCb.Items.Remove("SPEDITO");
                this.tableLayoutPanelSpedizione.Visible = false;
            }

            this.dataGridViewDettagliEStampe.DataSource = from p in context.DETTAGLIO_ORDINEs
                                                          where p.idOrdine == this.idOrdine
                                                          select new { p.STAMPA.tipo, p.STAMPA.tipoCarta, p.STAMPA.dimX, p.STAMPA.dimY, p.quantità, p.urlImmagine};
            this.dataGridViewDettagliEStampe.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            if(tipo == "C")
            {
                this.statoCb.Enabled = false;
                this.corriereBt.Hide();
            }
        }

        private void DataGridPagamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 5 || e.RowIndex < 0) { return; }
            if (dataGridViewPagamenti.Rows[e.RowIndex].Cells[2].Value == null)
            { 
                new EffettuaPagamento(Convert.ToInt32(dataGridViewPagamenti.Rows[e.RowIndex].Cells[0].Value), fattura.idFattura).ShowDialog();
                this.Dispose();
            }
        }

        private void statoCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            int INDICI = statoCb.Items.IndexOf(ordine.stato.TrimEnd()) +  1;
            if (statoCb.SelectedIndex != statoCb.Items.IndexOf(ordine.stato.TrimEnd()) + 1)
            {
                statoCb.Text = ordine.stato.TrimEnd();
                return;
            }
            else
            {
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                var ordineQuery =
                    from o in context.ORDINEs
                    where o.idOrdine == this.idOrdine
                    select o;
                foreach (var or in ordineQuery)
                {
                    or.stato = this.statoCb.SelectedItem.ToString();
                    if (statoCb.SelectedIndex == 3)
                    {
                        or.dataRitiro = DateTime.Now;
                    }
                    if (statoCb.SelectedIndex == 1)
                    {
                        or.dataEvasione = DateTime.Now;
                    }
                }
                context.SubmitChanges();
                this.Dispose();
            }
        }

        private void comboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void corriereBt_Click(object sender, EventArgs e)
        {
            new modificaCorriere(this.idOrdine).ShowDialog();
            this.Dispose();
        }
    }
}
