﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class gestioneDipendenti : Form
    {
        private int idCapo;
        public gestioneDipendenti(int idCapo)
        {
            InitializeComponent();
            this.idCapo = idCapo;
        }

        private void GestioneDipendenti_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.idTb.Text = NewIdDipendente(context).ToString();
            Sync_DataGridView(context);
            DataGridViewButtonColumn modbut = new DataGridViewButtonColumn();
            modbut.Name = "dataGridViewModifyButton";
            modbut.Text = "modifica";
            modbut.HeaderText = "modifica";
            modbut.Width = 20;
            modbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            modbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(modbut);
            DataGridViewButtonColumn delbut = new DataGridViewButtonColumn();
            delbut.Name = "dataGridViewDeleteButton";
            delbut.Text = "elimina";
            delbut.HeaderText = "elimina";
            delbut.Width = 20;
            delbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            delbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(delbut);
            dataGridView1.CellClick += DataGridView1_CellClick;
        }

        private static int NewIdDipendente(WhitePaperDataClassesDataContext context)
        {
            return (context.DIPENDENTEs.Select(x => (int?)x.idDipendente).Max() ?? 0) + 1;
        }

        private void Sync_DataGridView(WhitePaperDataClassesDataContext context)
        {
            dataGridView1.DataSource = from p in context.DIPENDENTEs
                                       where p.capo != '1'
                                       select new { p.idDipendente, p.CF, p.nome, p.cognome };
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 4 && e.ColumnIndex != 5) { return; }
            if (e.RowIndex < 0) return;
 
            if (e.ColumnIndex == 5)
            {
                if(MessageBox.Show("Sei sicuro di eliminare il dipendente " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                var ordini = from p in context.ORDINEs
                              where p.idDipendente == id
                              select p;
                if(ordini.Any())
                {
                    foreach (var ordine in ordini)
                    {
                        ordine.idDipendente = this.idCapo;
                    }
                }

                var listRes = from p in context.DIPENDENTEs
                              where p.idDipendente == id
                              select p;
                if (listRes.Count() > 0)
                {
                    context.DIPENDENTEs.DeleteOnSubmit(listRes.First());
                    context.SubmitChanges();
                }
                dataGridView1.Rows.RemoveAt(e.RowIndex);

            }
            else if(e.ColumnIndex == 4)
            {
                statusL.Text = "Modifica dipendente " + (dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                cfTb.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                nomeTb.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                cognomeTb.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                idTb.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                var listRes = from p in context.DIPENDENTEs
                              where p.idDipendente == id
                              select p.password;
                pswTb.Text = listRes.First();
                pswTb.UseSystemPasswordChar = false;
            }
        }

        private void Confirm_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(nomeTb.Text) || String.IsNullOrWhiteSpace(cognomeTb.Text) || String.IsNullOrWhiteSpace(cfTb.Text) ||
                String.IsNullOrWhiteSpace(pswTb.Text))
            {
                MessageBox.Show("Dati incompleti");
                return;
            }

            
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            if(context.DIPENDENTEs.Where(d => d.idDipendente != Convert.ToInt32(idTb.Text) && d.CF == cfTb.Text).Any())
            {
                MessageBox.Show("Dipendente già presente");
                return;
            }
            if (Convert.ToInt32(idTb.Text) < NewIdDipendente(context))
            {
                var dipendentiQuery =
                    from d in context.DIPENDENTEs
                    where d.idDipendente == Convert.ToInt32(idTb.Text)
                    select d;

                foreach (var dipendente in dipendentiQuery)
                {
                    dipendente.CF = cfTb.Text;
                    dipendente.nome = nomeTb.Text;
                    dipendente.cognome = cognomeTb.Text;
                    dipendente.password = pswTb.Text;
                    dipendente.idDipendente = Convert.ToInt32(idTb.Text);
                    if (checkBox1.CheckState == CheckState.Checked)
                    {
                        dipendente.capo = '1';
                    }
                    else
                    {
                        dipendente.capo = '0';
                    }
                }
            }
            else
            {
                DIPENDENTE dipendente = new DIPENDENTE();
                dipendente.CF = cfTb.Text;
                dipendente.nome = nomeTb.Text;
                dipendente.cognome = cognomeTb.Text;
                dipendente.password = pswTb.Text;
                dipendente.idDipendente = Convert.ToInt32(idTb.Text);
                if(checkBox1.CheckState == CheckState.Checked)
                {
                    dipendente.capo = '1';
                } else
                {
                    dipendente.capo = '0';
                }
                context.DIPENDENTEs.InsertOnSubmit(dipendente);
            }
            context.SubmitChanges();
            this.Dispose();
        }
    }
}
