﻿namespace whitePaper
{
    partial class FatturatoAnnuo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.annoUd = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.importoTb = new System.Windows.Forms.TextBox();
            this.Fatturato = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.annoUd)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // annoUd
            // 
            this.annoUd.Location = new System.Drawing.Point(159, 3);
            this.annoUd.Maximum = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            this.annoUd.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.annoUd.Name = "annoUd";
            this.annoUd.Size = new System.Drawing.Size(360, 31);
            this.annoUd.TabIndex = 0;
            this.annoUd.Value = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.annoUd.ValueChanged += new System.EventHandler(this.annoUd_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Anno";
            // 
            // importoTb
            // 
            this.importoTb.Location = new System.Drawing.Point(159, 53);
            this.importoTb.Name = "importoTb";
            this.importoTb.ReadOnly = true;
            this.importoTb.Size = new System.Drawing.Size(360, 31);
            this.importoTb.TabIndex = 2;
            // 
            // Fatturato
            // 
            this.Fatturato.AutoSize = true;
            this.Fatturato.Location = new System.Drawing.Point(3, 50);
            this.Fatturato.Name = "Fatturato";
            this.Fatturato.Size = new System.Drawing.Size(98, 25);
            this.Fatturato.TabIndex = 3;
            this.Fatturato.Text = "Fatturato";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.annoUd, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.importoTb, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Fatturato, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(87, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(522, 100);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // FatturatoAnnuo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(646, 228);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FatturatoAnnuo";
            this.Text = "Fatturato annuo";
            ((System.ComponentModel.ISupportInitialize)(this.annoUd)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown annoUd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox importoTb;
        private System.Windows.Forms.Label Fatturato;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}