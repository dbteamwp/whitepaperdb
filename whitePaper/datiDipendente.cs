﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class DatiDipendente : Form
    {
        int idDipendente;

        public DatiDipendente(int idDipendente)
        {
            InitializeComponent();
            this.idDipendente = idDipendente;
        }

        private void DatiDipendente_Load(object sender, EventArgs e)
        {
            titleL.Text += " " + idDipendente;
        }

        private void ConfirmBt_Click(object sender, EventArgs e)
        {
            if(newPsw2Tb.Text == newPswTb.Text && !String.IsNullOrWhiteSpace(newPswTb.Text))
            {
                //aggiorna
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                var dipendentiQuery =
                    from d in context.DIPENDENTEs
                    where d.idDipendente == this.idDipendente
                    select d;
                foreach (var dipendente in dipendentiQuery)
                {
                    dipendente.password = newPswTb.Text;
                }
                context.SubmitChanges();
                MessageBox.Show("Password modificata correttamente");
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Dati non corretti, riprova");
            }
        }

    }
}
