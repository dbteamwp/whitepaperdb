﻿namespace whitePaper
{
    partial class EffettuaPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tipoPagamentoLb = new System.Windows.Forms.ListBox();
            this.ConfirmBt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tipoPagamentoLb
            // 
            this.tipoPagamentoLb.FormattingEnabled = true;
            this.tipoPagamentoLb.ItemHeight = 25;
            this.tipoPagamentoLb.Location = new System.Drawing.Point(109, 34);
            this.tipoPagamentoLb.Name = "tipoPagamentoLb";
            this.tipoPagamentoLb.Size = new System.Drawing.Size(347, 79);
            this.tipoPagamentoLb.TabIndex = 0;
            // 
            // ConfirmBt
            // 
            this.ConfirmBt.Location = new System.Drawing.Point(509, 34);
            this.ConfirmBt.Name = "ConfirmBt";
            this.ConfirmBt.Size = new System.Drawing.Size(163, 79);
            this.ConfirmBt.TabIndex = 1;
            this.ConfirmBt.Text = "Conferma";
            this.ConfirmBt.UseVisualStyleBackColor = true;
            this.ConfirmBt.Click += new System.EventHandler(this.ConfirmBt_Click);
            // 
            // EffettuaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 136);
            this.Controls.Add(this.ConfirmBt);
            this.Controls.Add(this.tipoPagamentoLb);
            this.Name = "EffettuaPagamento";
            this.Text = "Pagamento";
            this.Load += new System.EventHandler(this.EffettuaPagamento_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox tipoPagamentoLb;
        private System.Windows.Forms.Button ConfirmBt;
    }
}