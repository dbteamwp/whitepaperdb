﻿namespace whitePaper.DatiPersonali
{
    partial class GestioneDatiHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modificaDatiButton = new System.Windows.Forms.Button();
            this.gestioneIndirizziButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // modificaDatiButton
            // 
            this.modificaDatiButton.Location = new System.Drawing.Point(12, 12);
            this.modificaDatiButton.Name = "modificaDatiButton";
            this.modificaDatiButton.Size = new System.Drawing.Size(309, 125);
            this.modificaDatiButton.TabIndex = 0;
            this.modificaDatiButton.Text = "Modifica i dati di accesso";
            this.modificaDatiButton.UseVisualStyleBackColor = true;
            this.modificaDatiButton.Click += new System.EventHandler(this.modificaDatiButton_Click);
            // 
            // gestioneIndirizziButton
            // 
            this.gestioneIndirizziButton.Location = new System.Drawing.Point(327, 12);
            this.gestioneIndirizziButton.Name = "gestioneIndirizziButton";
            this.gestioneIndirizziButton.Size = new System.Drawing.Size(309, 125);
            this.gestioneIndirizziButton.TabIndex = 1;
            this.gestioneIndirizziButton.Text = "Gestisci i tuoi indirizzi";
            this.gestioneIndirizziButton.UseVisualStyleBackColor = true;
            this.gestioneIndirizziButton.Click += new System.EventHandler(this.gestioneIndirizziButton_Click);
            // 
            // GestioneDatiHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(656, 153);
            this.Controls.Add(this.gestioneIndirizziButton);
            this.Controls.Add(this.modificaDatiButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GestioneDatiHome";
            this.Text = "Gestisci i tuoi dati";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button modificaDatiButton;
        private System.Windows.Forms.Button gestioneIndirizziButton;
    }
}