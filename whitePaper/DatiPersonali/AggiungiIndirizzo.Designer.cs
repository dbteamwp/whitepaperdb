﻿namespace whitePaper.DatiPersonali
{
    partial class AggiungiIndirizzo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fatturazioneCheckBox = new System.Windows.Forms.CheckBox();
            this.consegnaCheckBox = new System.Windows.Forms.CheckBox();
            this.residenzaCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cittàTextBox = new System.Windows.Forms.TextBox();
            this.viaTextBox = new System.Windows.Forms.TextBox();
            this.confermaButton = new System.Windows.Forms.Button();
            this.ncivicoTextBox = new System.Windows.Forms.TextBox();
            this.predefinitoCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // fatturazioneCheckBox
            // 
            this.fatturazioneCheckBox.AutoSize = true;
            this.fatturazioneCheckBox.Location = new System.Drawing.Point(35, 33);
            this.fatturazioneCheckBox.Name = "fatturazioneCheckBox";
            this.fatturazioneCheckBox.Size = new System.Drawing.Size(164, 29);
            this.fatturazioneCheckBox.TabIndex = 0;
            this.fatturazioneCheckBox.Text = "Fatturazione";
            this.fatturazioneCheckBox.UseVisualStyleBackColor = true;
            // 
            // consegnaCheckBox
            // 
            this.consegnaCheckBox.AutoSize = true;
            this.consegnaCheckBox.Location = new System.Drawing.Point(205, 33);
            this.consegnaCheckBox.Name = "consegnaCheckBox";
            this.consegnaCheckBox.Size = new System.Drawing.Size(142, 29);
            this.consegnaCheckBox.TabIndex = 1;
            this.consegnaCheckBox.Text = "Consegna";
            this.consegnaCheckBox.UseVisualStyleBackColor = true;
            // 
            // residenzaCheckBox
            // 
            this.residenzaCheckBox.AutoSize = true;
            this.residenzaCheckBox.Location = new System.Drawing.Point(353, 33);
            this.residenzaCheckBox.Name = "residenzaCheckBox";
            this.residenzaCheckBox.Size = new System.Drawing.Size(146, 29);
            this.residenzaCheckBox.TabIndex = 2;
            this.residenzaCheckBox.Text = "Residenza";
            this.residenzaCheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Città:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Via:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Numero civico:";
            // 
            // cittàTextBox
            // 
            this.cittàTextBox.Location = new System.Drawing.Point(205, 107);
            this.cittàTextBox.MaxLength = 20;
            this.cittàTextBox.Name = "cittàTextBox";
            this.cittàTextBox.Size = new System.Drawing.Size(462, 31);
            this.cittàTextBox.TabIndex = 6;
            // 
            // viaTextBox
            // 
            this.viaTextBox.Location = new System.Drawing.Point(205, 147);
            this.viaTextBox.MaxLength = 30;
            this.viaTextBox.Name = "viaTextBox";
            this.viaTextBox.Size = new System.Drawing.Size(462, 31);
            this.viaTextBox.TabIndex = 7;
            // 
            // confermaButton
            // 
            this.confermaButton.Location = new System.Drawing.Point(308, 328);
            this.confermaButton.Name = "confermaButton";
            this.confermaButton.Size = new System.Drawing.Size(207, 72);
            this.confermaButton.TabIndex = 9;
            this.confermaButton.Text = "OK";
            this.confermaButton.UseVisualStyleBackColor = true;
            this.confermaButton.Click += new System.EventHandler(this.confermaButton_Click);
            // 
            // ncivicoTextBox
            // 
            this.ncivicoTextBox.Location = new System.Drawing.Point(205, 187);
            this.ncivicoTextBox.MaxLength = 5;
            this.ncivicoTextBox.Name = "ncivicoTextBox";
            this.ncivicoTextBox.Size = new System.Drawing.Size(462, 31);
            this.ncivicoTextBox.TabIndex = 10;
            // 
            // predefinitoCheckBox
            // 
            this.predefinitoCheckBox.AutoSize = true;
            this.predefinitoCheckBox.Location = new System.Drawing.Point(205, 247);
            this.predefinitoCheckBox.Name = "predefinitoCheckBox";
            this.predefinitoCheckBox.Size = new System.Drawing.Size(231, 29);
            this.predefinitoCheckBox.TabIndex = 11;
            this.predefinitoCheckBox.Text = "Indirizzo predefinito";
            this.predefinitoCheckBox.UseVisualStyleBackColor = true;
            // 
            // AggiungiIndirizzo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(830, 463);
            this.Controls.Add(this.predefinitoCheckBox);
            this.Controls.Add(this.ncivicoTextBox);
            this.Controls.Add(this.confermaButton);
            this.Controls.Add(this.viaTextBox);
            this.Controls.Add(this.cittàTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.residenzaCheckBox);
            this.Controls.Add(this.consegnaCheckBox);
            this.Controls.Add(this.fatturazioneCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AggiungiIndirizzo";
            this.Text = "Aggiungi indirizzo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox fatturazioneCheckBox;
        private System.Windows.Forms.CheckBox consegnaCheckBox;
        private System.Windows.Forms.CheckBox residenzaCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cittàTextBox;
        private System.Windows.Forms.TextBox viaTextBox;
        private System.Windows.Forms.Button confermaButton;
        private System.Windows.Forms.TextBox ncivicoTextBox;
        private System.Windows.Forms.CheckBox predefinitoCheckBox;
    }
}