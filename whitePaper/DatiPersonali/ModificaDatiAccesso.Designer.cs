﻿namespace whitePaper.DatiPersonali
{
    partial class ModificaDatiAccesso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.newMailBox = new System.Windows.Forms.TextBox();
            this.newPwBox = new System.Windows.Forms.TextBox();
            this.pwAgainBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.confermaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nuova mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nuova password:";
            // 
            // newMailBox
            // 
            this.newMailBox.Location = new System.Drawing.Point(452, 37);
            this.newMailBox.MaxLength = 25;
            this.newMailBox.Name = "newMailBox";
            this.newMailBox.Size = new System.Drawing.Size(413, 31);
            this.newMailBox.TabIndex = 2;
            // 
            // newPwBox
            // 
            this.newPwBox.Location = new System.Drawing.Point(452, 87);
            this.newPwBox.MaxLength = 20;
            this.newPwBox.Name = "newPwBox";
            this.newPwBox.Size = new System.Drawing.Size(413, 31);
            this.newPwBox.TabIndex = 3;
            this.newPwBox.UseSystemPasswordChar = true;
            // 
            // pwAgainBox
            // 
            this.pwAgainBox.Location = new System.Drawing.Point(452, 136);
            this.pwAgainBox.MaxLength = 20;
            this.pwAgainBox.Name = "pwAgainBox";
            this.pwAgainBox.Size = new System.Drawing.Size(413, 31);
            this.pwAgainBox.TabIndex = 5;
            this.pwAgainBox.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(342, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Inserisci nuovamente la password:";
            // 
            // confermaButton
            // 
            this.confermaButton.Location = new System.Drawing.Point(316, 240);
            this.confermaButton.Name = "confermaButton";
            this.confermaButton.Size = new System.Drawing.Size(282, 112);
            this.confermaButton.TabIndex = 6;
            this.confermaButton.Text = "OK";
            this.confermaButton.UseVisualStyleBackColor = true;
            this.confermaButton.Click += new System.EventHandler(this.ConfermaButton_Click);
            // 
            // ModificaDatiAccesso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(924, 383);
            this.Controls.Add(this.confermaButton);
            this.Controls.Add(this.pwAgainBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.newPwBox);
            this.Controls.Add(this.newMailBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ModificaDatiAccesso";
            this.Text = "Modifica i tuoi dati di accesso";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox newMailBox;
        private System.Windows.Forms.TextBox newPwBox;
        private System.Windows.Forms.TextBox pwAgainBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button confermaButton;
    }
}