﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.DatiPersonali
{
    public partial class AggiungiIndirizzo : Form
    {

        private int _idCliente;

        public AggiungiIndirizzo(object sender, int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;

            if ((Form) sender is InserimentoOrdini.CompletaOrdine)
            {
                fatturazioneCheckBox.Checked = true;
                fatturazioneCheckBox.Enabled = false;
            }
        }

        private void confermaButton_Click(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            INDIRIZZO indirizzo = new INDIRIZZO();
            if(String.IsNullOrEmpty(cittàTextBox.Text) || String.IsNullOrEmpty(viaTextBox.Text)
                || String.IsNullOrEmpty(ncivicoTextBox.Text) 
                || (!fatturazioneCheckBox.Checked && !consegnaCheckBox.Checked && !residenzaCheckBox.Checked))
            {
                MessageBox.Show("Non ci sono dati suffienti per inserire l'indirizzo.");
                return;
            }
            indirizzo.idIndirizzo = (ctx.INDIRIZZOs.Select(i => (int?)i.idIndirizzo).Max() ?? 0) + 1;
            indirizzo.città = cittàTextBox.Text;
            indirizzo.via = viaTextBox.Text;
            indirizzo.numeroCivico = ncivicoTextBox.Text;
            indirizzo.Tipo_fatturazione = fatturazioneCheckBox.Checked ? '1' : '0';
            indirizzo.Tipo_consegna = consegnaCheckBox.Checked ? '1' : '0';
            indirizzo.Tipo_residenza = residenzaCheckBox.Checked ? '1' : '0';
            indirizzo.idCliente = _idCliente;
            indirizzo.predefinito = predefinitoCheckBox.Checked ? '1' : '0';

            ctx.INDIRIZZOs.InsertOnSubmit(indirizzo);
            ctx.SubmitChanges();

            Close();
        }
    }
}
