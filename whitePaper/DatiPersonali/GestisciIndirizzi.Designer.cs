﻿namespace whitePaper.DatiPersonali
{
    partial class GestisciIndirizzi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.indirizziGridView = new System.Windows.Forms.DataGridView();
            this.nuovoIndirizzoButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.indirizziGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // indirizziGridView
            // 
            this.indirizziGridView.AllowUserToAddRows = false;
            this.indirizziGridView.AllowUserToDeleteRows = false;
            this.indirizziGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.indirizziGridView.Location = new System.Drawing.Point(12, 106);
            this.indirizziGridView.Name = "indirizziGridView";
            this.indirizziGridView.ReadOnly = true;
            this.indirizziGridView.RowTemplate.Height = 33;
            this.indirizziGridView.Size = new System.Drawing.Size(1116, 629);
            this.indirizziGridView.TabIndex = 0;
            this.indirizziGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.indirizziGridView_CellContentClick);
            // 
            // nuovoIndirizzoButton
            // 
            this.nuovoIndirizzoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuovoIndirizzoButton.Location = new System.Drawing.Point(12, 12);
            this.nuovoIndirizzoButton.Name = "nuovoIndirizzoButton";
            this.nuovoIndirizzoButton.Size = new System.Drawing.Size(1116, 88);
            this.nuovoIndirizzoButton.TabIndex = 1;
            this.nuovoIndirizzoButton.Text = "Nuovo indirizzo";
            this.nuovoIndirizzoButton.UseVisualStyleBackColor = true;
            this.nuovoIndirizzoButton.Click += new System.EventHandler(this.nuovoIndirizzoButton_Click);
            // 
            // GestisciIndirizzi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1140, 747);
            this.Controls.Add(this.nuovoIndirizzoButton);
            this.Controls.Add(this.indirizziGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GestisciIndirizzi";
            this.Text = "Gestisci i tuoi indirizzi";
            ((System.ComponentModel.ISupportInitialize)(this.indirizziGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView indirizziGridView;
        private System.Windows.Forms.Button nuovoIndirizzoButton;
    }
}