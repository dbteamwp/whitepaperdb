﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.DatiPersonali
{
    public partial class ModificaDatiAccesso : Form
    {

        private int _idCliente;

        public ModificaDatiAccesso(int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;
        }

        private void ConfermaButton_Click(object sender, EventArgs e)
        {
            if (newPwBox.Text.Trim() != pwAgainBox.Text.Trim())
            {
                MessageBox.Show("Le password non coincidono!");
                return;
            }
            else if (String.IsNullOrWhiteSpace(newMailBox.Text) || String.IsNullOrWhiteSpace(newPwBox.Text) || String.IsNullOrWhiteSpace(pwAgainBox.Text))
            {
                MessageBox.Show("Inserire tutti i campi!");
                return;
            }

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var sameUserMail = from c in ctx.CLIENTEs
                               where c.mail == newMailBox.Text
                               && c.idCliente != this._idCliente
                               select c;

            if (sameUserMail.Any())
            {
                MessageBox.Show("Mail già presente.");
                return;
            }
            var clienteQuery = from c in ctx.CLIENTEs
                          where c.idCliente == _idCliente
                          select c;

            CLIENTE cliente = clienteQuery.First();
            cliente.mail = newMailBox.Text.Trim();
            cliente.password = newPwBox.Text.Trim();

            ctx.SubmitChanges();

            Close();
        }
    }
}
