﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.DatiPersonali
{
    public partial class GestisciIndirizzi : Form
    {

        int _idCliente;

        public GestisciIndirizzi(int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var indirizzi = from i in ctx.INDIRIZZOs
                            where i.idCliente == _idCliente
                            select new { i.predefinito, i.città, i.via, i.numeroCivico, i.Tipo_consegna, i.Tipo_fatturazione, i.Tipo_residenza, i.idIndirizzo };

            DataGridViewButtonColumn eliminaButtons = new DataGridViewButtonColumn();
            eliminaButtons.HeaderText = "Elimina indirizzo";
            eliminaButtons.Text = "Elimina";
            eliminaButtons.Name = "eliminaColumn";
            eliminaButtons.UseColumnTextForButtonValue = true;

            DataGridViewButtonColumn predefinitoButtons = new DataGridViewButtonColumn();
            predefinitoButtons.HeaderText = "Predefinito";
            predefinitoButtons.Text = "Predefinito";
            predefinitoButtons.Name = "predefinitoColumn";
            predefinitoButtons.UseColumnTextForButtonValue = true;

            indirizziGridView.Columns.AddRange(eliminaButtons, predefinitoButtons);
            indirizziGridView.DataSource = indirizzi;
        }

        private void nuovoIndirizzoButton_Click(object sender, EventArgs e)
        {
            var nuovoIndirizzo = new AggiungiIndirizzo(this, _idCliente);
            nuovoIndirizzo.ShowDialog();
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var indirizzi = from i in ctx.INDIRIZZOs
                            where i.idCliente == _idCliente
                            select new { i.predefinito, i.città, i.via, i.numeroCivico, i.Tipo_consegna, i.Tipo_fatturazione, i.Tipo_residenza, i.idIndirizzo };
            indirizziGridView.DataSource = indirizzi;
        }

        private void indirizziGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 1)
                {
                    WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
                    var indirizzo = from i in ctx.INDIRIZZOs
                                    where i.idIndirizzo == (int)senderGrid.Rows[e.RowIndex].Cells["idIndirizzo"].Value
                                    select i;
                    var newIndirizzo = indirizzo.First();
                    newIndirizzo.predefinito = '1';
                    ctx.SubmitChanges();
                }
                else if (e.ColumnIndex == 0)
                {
                    WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
                    var indirizzo = Convert.ToInt32(senderGrid.Rows[e.RowIndex].Cells["idIndirizzo"].Value);

                    if (ctx.FATTURAs.Where(f => f.idIndirizzo == indirizzo).Any() || ctx.SPEDIZIONEs.Where(s => s.idIndirizzo == indirizzo).Any())
                    {
                        MessageBox.Show("L'indirizzo è collegato a una spedizione o una fattura. Cancellazione fallita.");
                        UpdateGrid();
                        return;
                    }

                    Console.WriteLine(0);

                    ctx.INDIRIZZOs.DeleteOnSubmit(ctx.INDIRIZZOs.Where(i => i.idIndirizzo == indirizzo).First());
                    ctx.SubmitChanges();
                }

                UpdateGrid();
            }
        }
    }
}
