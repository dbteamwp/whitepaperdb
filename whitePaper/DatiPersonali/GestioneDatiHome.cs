﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.DatiPersonali
{
    public partial class GestioneDatiHome : Form
    {

        private int _idCliente;

        public GestioneDatiHome(int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;
        }

        private void modificaDatiButton_Click(object sender, EventArgs e)
        {
            new ModificaDatiAccesso(_idCliente).ShowDialog();
        }

        private void gestioneIndirizziButton_Click(object sender, EventArgs e)
        {
            var gestioneIndirizzi = new GestisciIndirizzi(_idCliente);
            gestioneIndirizzi.ShowDialog();
        }
    }
}
