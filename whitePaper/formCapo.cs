﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class formCapo : Form
    {
        private int idCapo;
        public formCapo(int idCapo)
        {
            this.idCapo = idCapo;
            InitializeComponent();
        }

        private void buttonDipendenti_Click(object sender, EventArgs e)
        {
            new gestioneDipendenti(this.idCapo).ShowDialog();
        }

        private void ListinoBt_Click(object sender, EventArgs e)
        {
            new GestioneStampe().ShowDialog();
        }

        private void corrieriBt_Click(object sender, EventArgs e)
        {
            new GestioneCorrieri().ShowDialog();
        }

        private void OrdiniBt_Click(object sender, EventArgs e)
        {
            new visualizzaOrdini(this.idCapo, "A").ShowDialog();
        }

        private void ClientiBt_Click(object sender, EventArgs e)
        {
            new GestisciClienti().ShowDialog();
        }

        private void PagamentiBt_Click(object sender, EventArgs e)
        {
            new gestioneTipoPagamenti().ShowDialog();
        }

        private void scadenzeBt_Click(object sender, EventArgs e)
        {
            new PagamentiForm(this.idCapo, "A").ShowDialog();
        }

        private void queryBt_Click(object sender, EventArgs e)
        {
            new query().ShowDialog();
        }

        private void fatturatoBt_Click(object sender, EventArgs e)
        {
            new FatturatoAnnuo().ShowDialog();
        }
    }
}
