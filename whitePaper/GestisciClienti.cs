﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class GestisciClienti : Form
    {
        public GestisciClienti()
        {
            InitializeComponent();
        }

        private void GestisciClienti_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            Sync_DataGridView(context);
            DataGridViewButtonColumn delbut = new DataGridViewButtonColumn();
            delbut.Name = "dataGridViewDeleteButton";
            delbut.Text = "elimina";
            delbut.HeaderText = "elimina";
            delbut.Width = 20;
            delbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            delbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(delbut);
            dataGridView1.CellClick += DataGridView1_CellClick;
            AutoSizeFormToDataGridView();
        }

        private void AutoSizeFormToDataGridView()
        {
            Size contentsSize = GetDataGridViewContentsSize();
            this.ClientSize = contentsSize;
        }


        protected Size GetDataGridViewContentsSize()
        {
            DockStyle dockStyleSave = dataGridView1.Dock;
            dataGridView1.Dock = DockStyle.None;
            dataGridView1.AutoSize = true;

            Size dataContentsSize = dataGridView1.Size;

            dataGridView1.AutoSize = false;
            dataGridView1.Dock = dockStyleSave;
            return dataContentsSize;
        }

        private void Sync_DataGridView(WhitePaperDataClassesDataContext context)
        {
            dataGridView1.DataSource = context.CLIENTEs.GetNewBindingList();
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 6) { return; }
            if (e.RowIndex < 0) return;

            if (MessageBox.Show("Sei sicuro di eliminare il cliente " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
            var listRes = from p in context.CLIENTEs
                            where p.idCliente == id
                            select p;
            if (listRes.Count() > 0)
            {
                context.CLIENTEs.DeleteOnSubmit(listRes.First());
                context.SubmitChanges();
            }
            dataGridView1.Rows.RemoveAt(e.RowIndex);
        }
    }
}
