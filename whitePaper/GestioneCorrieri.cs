﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class GestioneCorrieri : Form
    {
        public GestioneCorrieri()
        {
            InitializeComponent();
        }

        private void ConfirmBt_Click(object sender, EventArgs e)
        {
           if (String.IsNullOrWhiteSpace(nomeSocietàTb.Text) || String.IsNullOrWhiteSpace(telefonoTb.Text) ||
                Convert.ToInt32(telefonoTb.Text) <= 0)
            {
                MessageBox.Show("Dati incompleti");
                return;
            }

            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            if (Convert.ToInt32(idL.Text) < NewIdCorriere(context))
            {
                //modifica
                var corriereQuery =
                   from c in context.CORRIEREs
                   where c.idCorriere == Convert.ToInt32(idL.Text)
                   select c;

                foreach (var corriere in corriereQuery)
                {
                    corriere.telefono = Convert.ToInt32(telefonoTb.Text);
                    corriere.nomeSocietà = nomeSocietàTb.Text;
                    corriere.idCorriere= Convert.ToInt32(idL.Text);
                }
            }
            else
            {
                //inserisci
                CORRIERE corriere = new CORRIERE();
                corriere.telefono = Convert.ToInt32(telefonoTb.Text);
                corriere.nomeSocietà = nomeSocietàTb.Text;
                corriere.idCorriere = Convert.ToInt32(idL.Text);
                context.CORRIEREs.InsertOnSubmit(corriere);
            }
            context.SubmitChanges();
            this.Dispose();
        }

        private void txtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void GestioneCorrieri_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            this.idL.Text = NewIdCorriere(context).ToString();
            this.Sync_DataGridView(context);
            DataGridViewButtonColumn modbut = new DataGridViewButtonColumn();
            modbut.Name = "dataGridViewModifyButton";
            modbut.Text = "modifica";
            modbut.HeaderText = "modifica";
            modbut.Width = 20;
            modbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            modbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(modbut);
            DataGridViewButtonColumn delbut = new DataGridViewButtonColumn();
            delbut.Name = "dataGridViewDeleteButton";
            delbut.Text = "elimina";
            delbut.HeaderText = "elimina";
            delbut.Width = 20;
            delbut.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            delbut.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(delbut);
            dataGridView1.CellClick += DataGridView1_CellClick;
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3 && e.ColumnIndex != 4) { return; }
            if (e.RowIndex < 0) return;

            if (e.ColumnIndex == 4)
            {
                if (MessageBox.Show("Sei sicuro di eliminare il corriere " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
                WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
                var listRes = from p in context.CORRIEREs
                              where p.idCorriere == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value)
                              select p;
                if (listRes.Count() > 0)
                {
                    context.CORRIEREs.DeleteOnSubmit(listRes.First());
                    context.SubmitChanges();
                }
                dataGridView1.Rows.RemoveAt(e.RowIndex);

            }
            else if (e.ColumnIndex == 3)
            {
                idL.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                nomeSocietàTb.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                telefonoTb.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
        }

        private static int NewIdCorriere(WhitePaperDataClassesDataContext context)
        {
            return (context.CORRIEREs.Select(x => (int?)x.idCorriere).Max() ?? 0) + 1;
        }

        private void Sync_DataGridView(WhitePaperDataClassesDataContext context)
        {
            dataGridView1.DataSource = context.CORRIEREs.GetNewBindingList();
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }
    }
}
