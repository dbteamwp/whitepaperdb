﻿namespace whitePaper
{
    partial class GestioneCorrieri
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.corriereL = new System.Windows.Forms.Label();
            this.idL = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ConfirmBt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Telefono = new System.Windows.Forms.Label();
            this.nomeSocietàTb = new System.Windows.Forms.TextBox();
            this.telefonoTb = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // corriereL
            // 
            this.corriereL.AutoSize = true;
            this.corriereL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.corriereL.Location = new System.Drawing.Point(55, 83);
            this.corriereL.Name = "corriereL";
            this.corriereL.Size = new System.Drawing.Size(277, 31);
            this.corriereL.TabIndex = 3;
            this.corriereL.Text = "Gestione corriere ID";
            // 
            // idL
            // 
            this.idL.AutoSize = true;
            this.idL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idL.Location = new System.Drawing.Point(368, 83);
            this.idL.Name = "idL";
            this.idL.Size = new System.Drawing.Size(93, 33);
            this.idL.TabIndex = 19;
            this.idL.Text = "NULL";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(569, 74);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(946, 450);
            this.dataGridView1.TabIndex = 20;
            // 
            // ConfirmBt
            // 
            this.ConfirmBt.Location = new System.Drawing.Point(123, 436);
            this.ConfirmBt.Name = "ConfirmBt";
            this.ConfirmBt.Size = new System.Drawing.Size(274, 58);
            this.ConfirmBt.TabIndex = 21;
            this.ConfirmBt.Text = "Conferma";
            this.ConfirmBt.UseVisualStyleBackColor = true;
            this.ConfirmBt.Click += new System.EventHandler(this.ConfirmBt_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Telefono, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.nomeSocietàTb, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.telefonoTb, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(81, 179);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 194);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome società";
            // 
            // Telefono
            // 
            this.Telefono.AutoSize = true;
            this.Telefono.Location = new System.Drawing.Point(3, 83);
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(96, 25);
            this.Telefono.TabIndex = 1;
            this.Telefono.Text = "Telefono";
            // 
            // nomeSocietàTb
            // 
            this.nomeSocietàTb.Location = new System.Drawing.Point(130, 3);
            this.nomeSocietàTb.MaxLength = 20;
            this.nomeSocietàTb.Name = "nomeSocietàTb";
            this.nomeSocietàTb.Size = new System.Drawing.Size(292, 31);
            this.nomeSocietàTb.TabIndex = 2;
            // 
            // telefonoTb
            // 
            this.telefonoTb.Location = new System.Drawing.Point(130, 86);
            this.telefonoTb.Name = "telefonoTb";
            this.telefonoTb.Size = new System.Drawing.Size(292, 31);
            this.telefonoTb.TabIndex = 3;
            this.telefonoTb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBox_KeyPress);
            // 
            // GestioneCorrieri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1542, 557);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ConfirmBt);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.idL);
            this.Controls.Add(this.corriereL);
            this.Name = "GestioneCorrieri";
            this.Text = "Gestione corrieri";
            this.Load += new System.EventHandler(this.GestioneCorrieri_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label corriereL;
        private System.Windows.Forms.Label idL;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button ConfirmBt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Telefono;
        private System.Windows.Forms.TextBox nomeSocietàTb;
        private System.Windows.Forms.TextBox telefonoTb;
    }
}