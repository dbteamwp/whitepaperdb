﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class DettagliDipendente : Form
    {
        private int idDipendente;

        public DettagliDipendente(int id)
        {
            InitializeComponent();
            this.idDipendente = id;
        }

        private void DettagliDipendente_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            DIPENDENTE dipendente = context.DIPENDENTEs.Where(d => d.idDipendente == this.idDipendente).First();
            this.idDipendenteL.Text = dipendente.idDipendente.ToString();
            this.nomeTb.Text = dipendente.nome;
            this.cognomeTb.Text = dipendente.cognome;
            this.cfTb.Text = dipendente.CF;
        }
    }
}
