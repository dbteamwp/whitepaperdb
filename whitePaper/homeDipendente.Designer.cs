﻿namespace whitePaper
{
    partial class homeDipendente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.buttonCapo = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.datiPersonaliBt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pagamentiBt = new System.Windows.Forms.Button();
            this.scadenzaOrdiniBt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(381, 79);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ordini presi in carico";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // buttonCapo
            // 
            this.buttonCapo.Location = new System.Drawing.Point(426, 193);
            this.buttonCapo.Name = "buttonCapo";
            this.buttonCapo.Size = new System.Drawing.Size(381, 79);
            this.buttonCapo.TabIndex = 1;
            this.buttonCapo.Text = "Funzionalità capo";
            this.buttonCapo.UseVisualStyleBackColor = true;
            this.buttonCapo.Click += new System.EventHandler(this.Capo_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(426, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(381, 79);
            this.button3.TabIndex = 2;
            this.button3.Text = "Ordini da prendere in carico";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // datiPersonaliBt
            // 
            this.datiPersonaliBt.Location = new System.Drawing.Point(3, 193);
            this.datiPersonaliBt.Name = "datiPersonaliBt";
            this.datiPersonaliBt.Size = new System.Drawing.Size(381, 79);
            this.datiPersonaliBt.TabIndex = 3;
            this.datiPersonaliBt.Text = "Gestione dati personali";
            this.datiPersonaliBt.UseVisualStyleBackColor = true;
            this.datiPersonaliBt.Click += new System.EventHandler(this.DatiPersonaliBt_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonCapo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.datiPersonaliBt, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pagamentiBt, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.scadenzaOrdiniBt, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(18, 38);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33311F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(846, 287);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // pagamentiBt
            // 
            this.pagamentiBt.Location = new System.Drawing.Point(3, 98);
            this.pagamentiBt.Name = "pagamentiBt";
            this.pagamentiBt.Size = new System.Drawing.Size(381, 79);
            this.pagamentiBt.TabIndex = 4;
            this.pagamentiBt.Text = "Pagamenti in scadenza";
            this.pagamentiBt.UseVisualStyleBackColor = true;
            this.pagamentiBt.Click += new System.EventHandler(this.PagamentiBt_Click);
            // 
            // scadenzaOrdiniBt
            // 
            this.scadenzaOrdiniBt.Location = new System.Drawing.Point(426, 98);
            this.scadenzaOrdiniBt.Name = "scadenzaOrdiniBt";
            this.scadenzaOrdiniBt.Size = new System.Drawing.Size(381, 79);
            this.scadenzaOrdiniBt.TabIndex = 5;
            this.scadenzaOrdiniBt.Text = "Ordini in scadenza";
            this.scadenzaOrdiniBt.UseVisualStyleBackColor = true;
            this.scadenzaOrdiniBt.Click += new System.EventHandler(this.scadenzaOrdiniBt_Click);
            // 
            // homeDipendente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 362);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "homeDipendente";
            this.Text = "Home | Dipendente";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonCapo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button datiPersonaliBt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button pagamentiBt;
        private System.Windows.Forms.Button scadenzaOrdiniBt;
    }
}