﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class modificaCorriere : Form
    {
        private int idOrdine;
        public modificaCorriere(int idOrdine)
        {
            InitializeComponent();
            this.idOrdine = idOrdine;
        }

        private void modificaCorriere_Load(object sender, EventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            dataGridView1.DataSource = context.CORRIEREs.GetNewBindingList();
            DataGridViewButtonColumn seleziona = new DataGridViewButtonColumn();
            seleziona.Name = "dataGridViewSeleziona";
            seleziona.Text = "seleziona";
            seleziona.HeaderText = "seleziona";
            seleziona.Width = 20;
            seleziona.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            seleziona.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(seleziona);
            dataGridView1.CellClick += DataGrid_CellClick;
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void DataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3 || e.RowIndex < 0) { return; }
            if (MessageBox.Show("Sei sicuro di selezionare il corriere " + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();

            var ordineQuery =
               from o in context.ORDINEs
               where o.idOrdine == this.idOrdine
               select o;
           foreach (var or in ordineQuery)
           {
               int idCorriere = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
               or.SPEDIZIONEs.First().CORRIERE = context.CORRIEREs.Where(c => c.idCorriere == idCorriere).First();
               or.SPEDIZIONEs.First().idCorriere = idCorriere;
           }
           context.SubmitChanges();
            this.Dispose();
        }
    }
}
