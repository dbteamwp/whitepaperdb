﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Data;

namespace whitePaper
{
    public partial class visualizzaOrdini : Form
    {
        private int id;
        private string tipo;

        public visualizzaOrdini(int id, string tipo)
        {
            this.id = id;
            this.tipo = tipo;
            InitializeComponent();
        }

        private void visualizzaOrdini_Load(object sender, EventArgs e)
        {
            //mostra ordini in base a tipo e id
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            if (tipo == "D")
            {
                var res = context.ORDINEs.Where(o => o.idDipendente == this.id).Select(o => new { o.idOrdine, o.dataInserimento, o.stato }).OrderByDescending(o => o.dataInserimento).ToList();
                if (!res.Any())
                {
                    MessageBox.Show("Non ci sono dati da mostrare", "WhitePaper");
                    this.Hide();
                    this.Dispose();
                }
                // info cliente
                this.dataGridView1.DataSource = res;
            }

            else if (tipo == "C")
            {
                var res = context.ORDINEs.Where(o => o.idDipendente == null).Select(o => new { o.idOrdine, o.dataInserimento, o.stato }).OrderByDescending(o => o.dataInserimento).ToList();
                if (!res.Any())
                {
                    MessageBox.Show("Non ci sono dati da mostrare", "WhitePaper");
                    this.Hide();
                    this.Dispose();
                }
                // info cliente
                this.dataGridView1.DataSource = res;
            }

            else if (tipo == "A")
            {
                var res = context.ORDINEs.Select(o => new { o.idOrdine, o.dataInserimento, o.stato }).OrderByDescending(o => o.dataInserimento).ToList();
                if (!res.Any())
                {
                    MessageBox.Show("Non ci sono dati da mostrare", "WhitePaper");
                    this.Hide();
                    this.Dispose();
                }
                // info cliente
                this.dataGridView1.DataSource = res;
            }

            if (tipo == "S")
            {
                var res = context.ORDINEs
                    .Where(o => o.idDipendente == this.id
                    && o.dataEvasione == null)
                    .Select(o => new { o.idOrdine, o.dataScadenzaEvasione, o.stato })
                    .OrderByDescending(o => o.dataScadenzaEvasione).ToList();
                if (!res.Any())
                {
                    MessageBox.Show("Non ci sono dati da mostrare", "WhitePaper");
                    this.Hide();
                    this.Dispose();
                }
                // info cliente
                this.dataGridView1.DataSource = res;
            }

            if (tipo == "CLIENTE")
            {
                var res = context.ORDINEs.Where(o => o.idCliente == this.id).Select(o => new { o.idOrdine, o.dataInserimento, o.stato }).OrderByDescending(o => o.dataInserimento).ToList();
                if (!res.Any())
                {
                    MessageBox.Show("Non ci sono dati da mostrare", "WhitePaper");
                    this.Hide();
                    this.Dispose();
                }
                // info cliente
                this.dataGridView1.DataSource = res;
            }

            DataGridViewButtonColumn moreInfo = new DataGridViewButtonColumn();
            moreInfo.Name = "dataGridViewMoreInfoButton";
            moreInfo.Text = "dettagli";
            moreInfo.HeaderText = "dettagli";
            moreInfo.Width = 20;
            moreInfo.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            moreInfo.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(moreInfo);
            dataGridView1.CellClick += DataGridView1_CellClick;

            if(tipo == "CLIENTE")
            {
                DataGridViewButtonColumn fattura = new DataGridViewButtonColumn();
                fattura.Name = "dataGridViewClientInfo";
                fattura.Text = "fattura";
                fattura.HeaderText = "fattura";
                fattura.Width = 20;
                fattura.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                fattura.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(fattura);
            }

            if (tipo != "CLIENTE")
            {
                DataGridViewButtonColumn infoCliente = new DataGridViewButtonColumn();
                infoCliente.Name = "dataGridViewClientInfo";
                infoCliente.Text = "cliente";
                infoCliente.HeaderText = "cliente";
                infoCliente.Width = 20;
                infoCliente.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                infoCliente.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(infoCliente);
            }

            if (tipo == "A")
            {
                DataGridViewButtonColumn infoDipendente = new DataGridViewButtonColumn();
                infoDipendente.Name = "dataGridViewMoreInfoDipendente";
                infoDipendente.Text = "dipendente";
                infoDipendente.HeaderText = "dipendente";
                infoDipendente.Width = 20;
                infoDipendente.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                infoDipendente.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(infoDipendente);
            }

            if (tipo == "D" || tipo == "S")
            {
                DataGridViewButtonColumn nota = new DataGridViewButtonColumn();
                nota.Name = "dataGridViewAddNota";
                nota.Text = "aggiungi nota";
                nota.HeaderText = "aggiungi nota";
                nota.Width = 20;
                nota.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                nota.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(nota);
            }

            if (tipo == "C")
            {
                DataGridViewButtonColumn carico = new DataGridViewButtonColumn();
                carico.Name = "dataGridViewAddToYou";
                carico.Text = "prendi in carico";
                carico.HeaderText = "prendi in carico";
                carico.Width = 20;
                carico.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                carico.UseColumnTextForButtonValue = true;
                dataGridView1.Columns.Add(carico);
            }
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();

            if (e.ColumnIndex != 3 && e.ColumnIndex != 4 && e.ColumnIndex != 5) { return; }
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex == 3 && tipo != "CLIENTE")
            {
                new DettagliOrdine(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value), "D").ShowDialog();
                this.Dispose();
            }
            else if (e.ColumnIndex == 3 && tipo == "CLIENTE")
            {
                new DettagliOrdine(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value), "C").ShowDialog();
                this.Dispose();
            }
            else if (e.ColumnIndex == 4 && tipo != "CLIENTE")
            {
                new DettagliCliente(context.ORDINEs.Where(o => o.idOrdine == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value))
                    .Select(o => new { o.idCliente }).First().idCliente).ShowDialog();
            }
            else if (e.ColumnIndex == 4 && tipo == "CLIENTE")
            {
                //fattura
                ORDINE ordine = context.ORDINEs.Where(o => o.idOrdine == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value)).First();
                FATTURA fattura = context.FATTURAs.Where(f => f.idOrdine == ordine.idOrdine).First();
                if(context.PAGAMENTOs
                    .Where(p => p.idFattura == fattura.idFattura
                      && p.dataPagamento == null
                      && p.tipoPagamento == null)
                    .Any())
                {
                    MessageBox.Show("Fattura non disponibile al momento, bisogna effettuare tutti i pagamenti.");
                    return;
                }
                FileStream fs = new FileStream("fattura" + ordine.idOrdine + "_" + fattura.idFattura + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                Document doc = new Document();
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.Open();
                doc.Add(new Paragraph("        WHITE PAPER "));
                doc.Add(new Paragraph(fattura.ORDINE.CLIENTE.ragioneSociale.TrimEnd() + " - " + fattura.INDIRIZZO.città.TrimEnd() + " " + fattura.INDIRIZZO.via.TrimEnd() + ", " + fattura.INDIRIZZO.numeroCivico.TrimEnd()));
                if (fattura.ORDINE.CLIENTE.Tipo_azienda == '1')
                {
                    doc.Add(new Paragraph("PIVA " + fattura.ORDINE.CLIENTE.CF_PIVA.TrimEnd()));
                }
                else
                {
                    doc.Add(new Paragraph("CF " + fattura.ORDINE.CLIENTE.CF_PIVA.TrimEnd()));
                }
                doc.Add(new Paragraph(" "));

                PdfPTable table = new PdfPTable(5);
                float[] widths = new float[] { 1f, 1f, 1f, 1f, 1f };
                table.TotalWidth = 500f;
                table.LockedWidth = true;
                table.SetWidths(widths);
                table.AddCell(new PdfPCell(new Phrase("Tipo")));
                table.AddCell(new PdfPCell(new Phrase("Tipo carta")));
                table.AddCell(new PdfPCell(new Phrase("Dim X")));
                table.AddCell(new PdfPCell(new Phrase("Dim Y")));
                table.AddCell(new PdfPCell(new Phrase("Quantità")));

                foreach(DETTAGLIO_ORDINE d in ordine.DETTAGLIO_ORDINEs)
                {
                    table.AddCell(d.STAMPA.tipo);
                    table.AddCell(d.STAMPA.tipoCarta);
                    table.AddCell(d.STAMPA.dimX.ToString());
                    table.AddCell(d.STAMPA.dimY.ToString());
                    table.AddCell(d.quantità.ToString());
                }
                doc.Add(table);
                doc.Add(new Paragraph("TOTALE : " + fattura.importo + "€"));
                doc.Add(new Paragraph(" "));
                doc.Add(new Paragraph(fattura.dataEmissione?.ToString("dd/MM/yyyy")));
                doc.Close();
                MessageBox.Show("Fattura salvata nella cartella corrente.");
            }
            else if (e.ColumnIndex == 5 && (tipo == "D" || tipo == "S"))
            {
                new NoteOrdine(Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value)).ShowDialog();
            }
            else if (e.ColumnIndex == 5 && tipo == "A")
            {
                if(context.ORDINEs.Where(o => o.idOrdine == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value) 
                && o.idDipendente != null)
                    .Select(o => new { o.idDipendente }).Any())
                {
                    new DettagliDipendente(Convert.ToInt32(context.ORDINEs.Where(o => o.idOrdine == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value))
                    .Select(o => new { o.idDipendente }).First().idDipendente)).ShowDialog();
                }
            }
            else if (e.ColumnIndex == 5 && tipo == "C")
            {
                if(MessageBox.Show("Sei sicuro di prenderti in carico l'ordine selezionato?", "White Paper", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var ordineQuery = from o in context.ORDINEs
                                  where o.idOrdine == Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value)
                                  select o;
                    foreach (var or in ordineQuery)
                    {
                        or.idDipendente = this.id;
                        or.DIPENDENTE = context.DIPENDENTEs.Where(d => d.idDipendente == id).First();
                    }
                    context.SubmitChanges();
                    this.Dispose();
                }
            }
        }
    }
}
