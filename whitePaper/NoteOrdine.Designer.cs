﻿namespace whitePaper
{
    partial class NoteOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notaTb = new System.Windows.Forms.TextBox();
            this.NotaBt = new System.Windows.Forms.Button();
            this.dataGridViewNote = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNote)).BeginInit();
            this.SuspendLayout();
            // 
            // notaTb
            // 
            this.notaTb.Location = new System.Drawing.Point(32, 296);
            this.notaTb.Multiline = true;
            this.notaTb.Name = "notaTb";
            this.notaTb.Size = new System.Drawing.Size(587, 149);
            this.notaTb.TabIndex = 22;
            // 
            // NotaBt
            // 
            this.NotaBt.Location = new System.Drawing.Point(635, 296);
            this.NotaBt.Name = "NotaBt";
            this.NotaBt.Size = new System.Drawing.Size(165, 149);
            this.NotaBt.TabIndex = 21;
            this.NotaBt.Text = "Aggiungi nota";
            this.NotaBt.UseVisualStyleBackColor = true;
            this.NotaBt.Click += new System.EventHandler(this.NotaBt_Click);
            // 
            // dataGridViewNote
            // 
            this.dataGridViewNote.AllowUserToAddRows = false;
            this.dataGridViewNote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNote.Location = new System.Drawing.Point(32, 70);
            this.dataGridViewNote.Name = "dataGridViewNote";
            this.dataGridViewNote.ReadOnly = true;
            this.dataGridViewNote.RowTemplate.Height = 33;
            this.dataGridViewNote.Size = new System.Drawing.Size(768, 203);
            this.dataGridViewNote.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(25, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 37);
            this.label8.TabIndex = 19;
            this.label8.Text = "NOTE";
            // 
            // NoteOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 532);
            this.Controls.Add(this.notaTb);
            this.Controls.Add(this.NotaBt);
            this.Controls.Add(this.dataGridViewNote);
            this.Controls.Add(this.label8);
            this.Name = "NoteOrdine";
            this.Text = "Note ordine";
            this.Load += new System.EventHandler(this.NoteOrdine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNote)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox notaTb;
        private System.Windows.Forms.Button NotaBt;
        private System.Windows.Forms.DataGridView dataGridViewNote;
        private System.Windows.Forms.Label label8;
    }
}