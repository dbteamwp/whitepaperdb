﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class ComposizioneDettaglio : Form
    {

        public string Url { get; set; }
        public int Quantity { get; set; }

        public ComposizioneDettaglio()
        {
            InitializeComponent();
            Quantity = 1;
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void confermaDettaglioButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(urlBox.Text))
            {
                Url = urlBox.Text.Trim();
                Quantity = Decimal.ToInt32(quantityBox.Value);
                Close();
            }
        }
    }
}
