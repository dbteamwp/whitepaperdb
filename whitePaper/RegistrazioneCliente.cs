﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class RegistrazioneCliente : Form
    {

        public RegistrazioneCliente()
        {
            InitializeComponent();
        }

        private void AccediLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Close();
        }

        private void AziendaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ragioneSocialeLabel.Text = aziendaCheckBox.Checked ? "Ragione sociale:" : "Nome e cognome:";
            CFLabel.Text = aziendaCheckBox.Checked ? "Partita IVA:" : "Codice Fiscale:";
        }

        private void RegistraButton_Click(object sender, EventArgs e)
        {
            if (AnyBoxEmpty(ragioneSocialeBox, CFBox, mailBox, pwBox, pwAgainBox))
            {
                correctFieldsLabel.Text = "Inserisci tutti i dati!";
                correctFieldsLabel.Visible = true;
                return;
            }
            else if (pwAgainBox.Text != pwBox.Text)
            {
                correctFieldsLabel.Visible = true;
                correctFieldsLabel.Text = "Le password non coincidono!";
                return;
            }

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var sameUser = from c in ctx.CLIENTEs
                           where c.CF_PIVA == CFBox.Text
                           select c;

            if (sameUser.Any())
            {
                correctFieldsLabel.Visible = true;
                correctFieldsLabel.Text = "Utente già presente!";
                return;
            }

            var sameUserMail = from c in ctx.CLIENTEs
                           where c.mail == mailBox.Text
                           select c;

            if (sameUserMail.Any())
            {
                correctFieldsLabel.Visible = true;
                correctFieldsLabel.Text = "Mail già presente!";
                return;
            }

            using (WhitePaperDataClassesDataContext newCtx = new WhitePaperDataClassesDataContext())
            {

                var newCustomer = new CLIENTE()
                {
                    idCliente = (newCtx.CLIENTEs.Select(c => (int?)c.idCliente).Max() ?? 0) + 1,
                    ragioneSociale = ragioneSocialeBox.Text,
                    CF_PIVA = CFBox.Text,
                    mail = mailBox.Text,
                    password = pwBox.Text,
                    Tipo_azienda = aziendaCheckBox.Checked ? '1' : '0'
                };

                // Inserisci il nuovo cliente
                newCtx.CLIENTEs.InsertOnSubmit(newCustomer);
                newCtx.SubmitChanges();
                this.Dispose();
            }
        }

        private bool AnyBoxEmpty(params TextBox[] boxes)
        {
            return boxes.Where(b => String.IsNullOrWhiteSpace(b.Text)).Count() > 0;
        }
    }
}
