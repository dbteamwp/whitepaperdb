﻿namespace whitePaper
{
    partial class RegistrazioneCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accediLabel = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.registraButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pwBox = new System.Windows.Forms.TextBox();
            this.mailBox = new System.Windows.Forms.TextBox();
            this.pwAgainBox = new System.Windows.Forms.TextBox();
            this.nuovamenteLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CFLabel = new System.Windows.Forms.Label();
            this.ragioneSocialeLabel = new System.Windows.Forms.Label();
            this.CFBox = new System.Windows.Forms.TextBox();
            this.ragioneSocialeBox = new System.Windows.Forms.TextBox();
            this.aziendaCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.correctFieldsLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // accediLabel
            // 
            this.accediLabel.AutoSize = true;
            this.accediLabel.Location = new System.Drawing.Point(414, 731);
            this.accediLabel.Name = "accediLabel";
            this.accediLabel.Size = new System.Drawing.Size(83, 25);
            this.accediLabel.TabIndex = 13;
            this.accediLabel.TabStop = true;
            this.accediLabel.Text = "Accedi!";
            this.accediLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AccediLabel_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 731);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Sei già registrato?";
            // 
            // registraButton
            // 
            this.registraButton.Location = new System.Drawing.Point(259, 615);
            this.registraButton.Name = "registraButton";
            this.registraButton.Size = new System.Drawing.Size(199, 83);
            this.registraButton.TabIndex = 11;
            this.registraButton.Text = "Registrati";
            this.registraButton.UseVisualStyleBackColor = true;
            this.registraButton.Click += new System.EventHandler(this.RegistraButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Mail:";
            // 
            // pwBox
            // 
            this.pwBox.Location = new System.Drawing.Point(372, 126);
            this.pwBox.MaxLength = 20;
            this.pwBox.Name = "pwBox";
            this.pwBox.Size = new System.Drawing.Size(357, 31);
            this.pwBox.TabIndex = 8;
            this.pwBox.UseSystemPasswordChar = true;
            // 
            // mailBox
            // 
            this.mailBox.Location = new System.Drawing.Point(372, 55);
            this.mailBox.MaxLength = 25;
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(357, 31);
            this.mailBox.TabIndex = 7;
            // 
            // pwAgainBox
            // 
            this.pwAgainBox.Location = new System.Drawing.Point(372, 188);
            this.pwAgainBox.MaxLength = 20;
            this.pwAgainBox.Name = "pwAgainBox";
            this.pwAgainBox.Size = new System.Drawing.Size(357, 31);
            this.pwAgainBox.TabIndex = 14;
            this.pwAgainBox.UseSystemPasswordChar = true;
            // 
            // nuovamenteLabel
            // 
            this.nuovamenteLabel.AutoSize = true;
            this.nuovamenteLabel.Location = new System.Drawing.Point(24, 191);
            this.nuovamenteLabel.Name = "nuovamenteLabel";
            this.nuovamenteLabel.Size = new System.Drawing.Size(342, 25);
            this.nuovamenteLabel.TabIndex = 15;
            this.nuovamenteLabel.Text = "Inserisci nuovamente la password:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CFLabel);
            this.groupBox1.Controls.Add(this.ragioneSocialeLabel);
            this.groupBox1.Controls.Add(this.CFBox);
            this.groupBox1.Controls.Add(this.ragioneSocialeBox);
            this.groupBox1.Controls.Add(this.aziendaCheckBox);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(769, 229);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dati anagrafici";
            // 
            // CFLabel
            // 
            this.CFLabel.AutoSize = true;
            this.CFLabel.Location = new System.Drawing.Point(27, 150);
            this.CFLabel.Name = "CFLabel";
            this.CFLabel.Size = new System.Drawing.Size(153, 25);
            this.CFLabel.TabIndex = 5;
            this.CFLabel.Text = "Codice fiscale:";
            // 
            // ragioneSocialeLabel
            // 
            this.ragioneSocialeLabel.AutoSize = true;
            this.ragioneSocialeLabel.Location = new System.Drawing.Point(27, 91);
            this.ragioneSocialeLabel.Name = "ragioneSocialeLabel";
            this.ragioneSocialeLabel.Size = new System.Drawing.Size(186, 25);
            this.ragioneSocialeLabel.TabIndex = 4;
            this.ragioneSocialeLabel.Text = "Nome e cognome:";
            // 
            // CFBox
            // 
            this.CFBox.Location = new System.Drawing.Point(246, 147);
            this.CFBox.MaxLength = 15;
            this.CFBox.Name = "CFBox";
            this.CFBox.Size = new System.Drawing.Size(333, 31);
            this.CFBox.TabIndex = 2;
            // 
            // ragioneSocialeBox
            // 
            this.ragioneSocialeBox.Location = new System.Drawing.Point(246, 91);
            this.ragioneSocialeBox.MaxLength = 20;
            this.ragioneSocialeBox.Name = "ragioneSocialeBox";
            this.ragioneSocialeBox.Size = new System.Drawing.Size(333, 31);
            this.ragioneSocialeBox.TabIndex = 1;
            // 
            // aziendaCheckBox
            // 
            this.aziendaCheckBox.AutoSize = true;
            this.aziendaCheckBox.Location = new System.Drawing.Point(32, 41);
            this.aziendaCheckBox.Name = "aziendaCheckBox";
            this.aziendaCheckBox.Size = new System.Drawing.Size(204, 29);
            this.aziendaCheckBox.TabIndex = 0;
            this.aziendaCheckBox.Text = "Sono un\'azienda";
            this.aziendaCheckBox.UseVisualStyleBackColor = true;
            this.aziendaCheckBox.CheckedChanged += new System.EventHandler(this.AziendaCheckBox_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mailBox);
            this.groupBox2.Controls.Add(this.pwBox);
            this.groupBox2.Controls.Add(this.nuovamenteLabel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.pwAgainBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(13, 264);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(769, 281);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Credenziali d\'accesso";
            // 
            // correctFieldsLabel
            // 
            this.correctFieldsLabel.AutoSize = true;
            this.correctFieldsLabel.ForeColor = System.Drawing.Color.Crimson;
            this.correctFieldsLabel.Location = new System.Drawing.Point(268, 560);
            this.correctFieldsLabel.Name = "correctFieldsLabel";
            this.correctFieldsLabel.Size = new System.Drawing.Size(190, 25);
            this.correctFieldsLabel.TabIndex = 18;
            this.correctFieldsLabel.Text = "Inserisci tutti i dati!";
            this.correctFieldsLabel.Visible = false;
            // 
            // RegistrazioneCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 781);
            this.Controls.Add(this.correctFieldsLabel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.accediLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.registraButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "RegistrazioneCliente";
            this.Text = "Registrazione cliente";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel accediLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button registraButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pwBox;
        private System.Windows.Forms.TextBox mailBox;
        private System.Windows.Forms.TextBox pwAgainBox;
        private System.Windows.Forms.Label nuovamenteLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox aziendaCheckBox;
        private System.Windows.Forms.TextBox CFBox;
        private System.Windows.Forms.TextBox ragioneSocialeBox;
        private System.Windows.Forms.Label CFLabel;
        private System.Windows.Forms.Label ragioneSocialeLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label correctFieldsLabel;
    }
}