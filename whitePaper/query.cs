﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class query : Form
    {
        public query()
        {
            InitializeComponent();
        }

        private void TopDipendentiBt_Click(object sender, EventArgs e)
        {
            this.clear();
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();

            dataGridView1.DataSource = context.DIPENDENTEs
                .Select(d => new { d.idDipendente,
                    d.nome,
                    d.cognome,
                    d.CF,
                    Ordini = d.ORDINEs.Count})
                .OrderByDescending(d => d.Ordini).Take(3);
            
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void ClientiBt_Click(object sender, EventArgs e)
        {
            this.clear();
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            dataGridView1.DataSource = (from o in context.ORDINEs
                                       from f in context.FATTURAs
                                       where f.idOrdine == o.idOrdine
                                       group f by o.CLIENTE into q
                                       select new { q.Key.idCliente,
                                                    q.Key.ragioneSociale,
                                                    q.Key.CF_PIVA,
                                                    q.Key.mail,
                                                    Importo_Totale = q.Sum(f => f.importo) }).Take(10);

            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void clear()
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();
            this.dataGridView1.Columns.Clear();
        }

        private void ClientiBt2_Click(object sender, EventArgs e)
        {
            this.clear();
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            dataGridView1.DataSource = context.CLIENTEs
               .Select(c => new { c.idCliente,
                   c.ragioneSociale,
                   c.CF_PIVA,
                   c.mail,
                   Ordini = c.ORDINEs.Count })
               .OrderByDescending(c => c.Ordini).Take(10);

            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        private void StampeBt_Click(object sender, EventArgs e)
        {
            this.clear();
            WhitePaperDataClassesDataContext context = new WhitePaperDataClassesDataContext();
            dataGridView1.DataSource = (from s in context.STAMPAs
                                        from dO in context.DETTAGLIO_ORDINEs
                                        where s.ID == dO.idStampa
                                        group dO by s into q
                                        select new { q.Key.ID,
                                            prezzo_attuale = q.Key.prezzo * q.Key.sconto,
                                            q.Key.tipo,
                                            q.Key.tipoCarta,
                                            q.Key.dimX,
                                            q.Key.dimY,
                                            Quantità = q.Sum(f => f.quantità) }).Take(10);

            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }
    }
}
