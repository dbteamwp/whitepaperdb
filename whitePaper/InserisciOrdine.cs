﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class InserisciOrdine : Form
    {

        private DataTable _dt;

        private int _idCliente;
        private int? _numeroPagamenti;
        private int? _idIndirizzo;
        public float CurrentPrice { get; private set; }
        public float ShippingCost { get; private set; }

        public InserisciOrdine(int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var listino = from s in ctx.STAMPAs
                          select s;
            listinoGridView.DataSource = listino;

            DataGridViewButtonColumn addProductButtons = new DataGridViewButtonColumn();
            listinoGridView.Columns.Add(addProductButtons);
            addProductButtons.HeaderText = "Aggiungi prodotto";
            addProductButtons.Text = "Aggiungi";
            addProductButtons.Name = "addProductColumn";
            addProductButtons.UseColumnTextForButtonValue = true;

            _dt = new DataTable();
            _dt.Columns.Add(new DataColumn("id stampa", typeof(int)));
            _dt.Columns.Add(new DataColumn("tipo stampa", typeof(string)));
            _dt.Columns.Add(new DataColumn("dimX", typeof(float)));
            _dt.Columns.Add(new DataColumn("dimY", typeof(float)));
            _dt.Columns.Add(new DataColumn("quantità", typeof(int)));
            _dt.Columns.Add(new DataColumn("URL", typeof(string)));
            _dt.Columns.Add(new DataColumn("prezzo", typeof(float)));
            dettagliGridView.DataSource = _dt;

            CurrentPrice = 0;
        }

        private void listinoGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var detailComp = new ComposizioneDettaglio();
                detailComp.ShowDialog();

                _dt.Rows.Add(listinoGridView.Rows[e.RowIndex].Cells["ID"].Value,
                    listinoGridView.Rows[e.RowIndex].Cells["tipo"].Value,
                    listinoGridView.Rows[e.RowIndex].Cells["dimX"].Value, 
                    listinoGridView.Rows[e.RowIndex].Cells["dimY"].Value, 
                    detailComp.Quantity, 
                    detailComp.Url,
                    (float) listinoGridView.Rows[e.RowIndex].Cells["prezzo"].Value * 
                        (1 - ((float?)listinoGridView.Rows[e.RowIndex].Cells["sconto"].Value ?? 0)) * 
                        detailComp.Quantity);

                CurrentPrice = 0;
                foreach (var row in dettagliGridView.Rows.Cast<DataGridViewRow>())
                {
                    CurrentPrice += (float) row.Cells["prezzo"].Value;
                }

                priceLabel.Text = CurrentPrice + "€";
            }

            CheckOrderSubmittable();
        }

        private void pulisciListaButton_Click(object sender, EventArgs e)
        {
            _dt.Clear();
            ResetCurrentPrice();
        }

        private void ResetCurrentPrice()
        {
            CurrentPrice = 0;
            priceLabel.Text = "0 €";
            CheckOrderSubmittable();
        }

        private void confermaOrdineButton_Click(object sender, EventArgs e)
        {
            var inoltra = new InserimentoOrdini.CompletaOrdine(_idCliente, _numeroPagamenti.Value, _idIndirizzo, _dt, CurrentPrice, ShippingCost);
            Console.WriteLine(_idIndirizzo);
            inoltra.ShowDialog();
            Close();
        }

        private void pagamentoButton_Click(object sender, EventArgs e)
        {
            var pagamenti = new ScegliModPagamento(CurrentPrice);
            pagamenti.ShowDialog();
            _numeroPagamenti = pagamenti.NumeroPagamenti ?? _numeroPagamenti;

            CheckOrderSubmittable();
        }

        private void CheckOrderSubmittable()
        {
            if (CurrentPrice != 0 && _numeroPagamenti.HasValue)
            {
                confermaOrdineButton.Enabled = true;
            }
            else
            {
                confermaOrdineButton.Enabled = false;
            }
        }

        private void consegnaButton_Click(object sender, EventArgs e)
        {
            var indirizzi = new InserimentoOrdini.ScegliIndirizzo(_idCliente);
            indirizzi.ShowDialog();
            _idIndirizzo = indirizzi.ChosenAddressIndex;
            if (indirizzi.ShippingCost.HasValue)
            {
                ShippingCost = indirizzi.ShippingCost.Value;
            }
        }
    }
}
