﻿namespace whitePaper
{
    partial class InserisciOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listinoGridView = new System.Windows.Forms.DataGridView();
            this.dettagliGridView = new System.Windows.Forms.DataGridView();
            this.selectedLabel = new System.Windows.Forms.Label();
            this.listinoProdotti = new System.Windows.Forms.Label();
            this.pulisciListaButton = new System.Windows.Forms.Button();
            this.consegnaButton = new System.Windows.Forms.Button();
            this.pagamentoButton = new System.Windows.Forms.Button();
            this.confermaOrdineButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.listinoGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dettagliGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // listinoGridView
            // 
            this.listinoGridView.AllowUserToAddRows = false;
            this.listinoGridView.AllowUserToDeleteRows = false;
            this.listinoGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listinoGridView.Location = new System.Drawing.Point(740, 41);
            this.listinoGridView.Name = "listinoGridView";
            this.listinoGridView.ReadOnly = true;
            this.listinoGridView.RowTemplate.Height = 33;
            this.listinoGridView.Size = new System.Drawing.Size(1702, 1049);
            this.listinoGridView.TabIndex = 0;
            this.listinoGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listinoGridView_CellContentClick);
            // 
            // dettagliGridView
            // 
            this.dettagliGridView.AllowUserToAddRows = false;
            this.dettagliGridView.AllowUserToDeleteRows = false;
            this.dettagliGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dettagliGridView.Location = new System.Drawing.Point(12, 41);
            this.dettagliGridView.Name = "dettagliGridView";
            this.dettagliGridView.ReadOnly = true;
            this.dettagliGridView.RowTemplate.Height = 33;
            this.dettagliGridView.Size = new System.Drawing.Size(722, 584);
            this.dettagliGridView.TabIndex = 1;
            // 
            // selectedLabel
            // 
            this.selectedLabel.AutoSize = true;
            this.selectedLabel.Location = new System.Drawing.Point(13, 13);
            this.selectedLabel.Name = "selectedLabel";
            this.selectedLabel.Size = new System.Drawing.Size(195, 25);
            this.selectedLabel.TabIndex = 2;
            this.selectedLabel.Text = "Prodotti selezionati";
            // 
            // listinoProdotti
            // 
            this.listinoProdotti.AutoSize = true;
            this.listinoProdotti.Location = new System.Drawing.Point(740, 13);
            this.listinoProdotti.Name = "listinoProdotti";
            this.listinoProdotti.Size = new System.Drawing.Size(153, 25);
            this.listinoProdotti.TabIndex = 3;
            this.listinoProdotti.Text = "Listino prodotti";
            // 
            // pulisciListaButton
            // 
            this.pulisciListaButton.Location = new System.Drawing.Point(530, 631);
            this.pulisciListaButton.Name = "pulisciListaButton";
            this.pulisciListaButton.Size = new System.Drawing.Size(204, 67);
            this.pulisciListaButton.TabIndex = 4;
            this.pulisciListaButton.Text = "Pulisci lista";
            this.pulisciListaButton.UseVisualStyleBackColor = true;
            this.pulisciListaButton.Click += new System.EventHandler(this.pulisciListaButton_Click);
            // 
            // consegnaButton
            // 
            this.consegnaButton.Location = new System.Drawing.Point(89, 840);
            this.consegnaButton.Name = "consegnaButton";
            this.consegnaButton.Size = new System.Drawing.Size(267, 75);
            this.consegnaButton.TabIndex = 5;
            this.consegnaButton.Text = "Scegli metodo di consegna";
            this.consegnaButton.UseVisualStyleBackColor = true;
            this.consegnaButton.Click += new System.EventHandler(this.consegnaButton_Click);
            // 
            // pagamentoButton
            // 
            this.pagamentoButton.Location = new System.Drawing.Point(362, 840);
            this.pagamentoButton.Name = "pagamentoButton";
            this.pagamentoButton.Size = new System.Drawing.Size(267, 75);
            this.pagamentoButton.TabIndex = 6;
            this.pagamentoButton.Text = "Scegli modalità di pagamento";
            this.pagamentoButton.UseVisualStyleBackColor = true;
            this.pagamentoButton.Click += new System.EventHandler(this.pagamentoButton_Click);
            // 
            // confermaOrdineButton
            // 
            this.confermaOrdineButton.Enabled = false;
            this.confermaOrdineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confermaOrdineButton.Location = new System.Drawing.Point(89, 921);
            this.confermaOrdineButton.Name = "confermaOrdineButton";
            this.confermaOrdineButton.Size = new System.Drawing.Size(540, 158);
            this.confermaOrdineButton.TabIndex = 7;
            this.confermaOrdineButton.Text = "Conferma ordine";
            this.confermaOrdineButton.UseVisualStyleBackColor = true;
            this.confermaOrdineButton.Click += new System.EventHandler(this.confermaOrdineButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(221, 783);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 31);
            this.label1.TabIndex = 8;
            this.label1.Text = "TOTALE:";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(362, 783);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(51, 31);
            this.priceLabel.TabIndex = 9;
            this.priceLabel.Text = "0 €";
            // 
            // InserisciOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2454, 1102);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.confermaOrdineButton);
            this.Controls.Add(this.pagamentoButton);
            this.Controls.Add(this.consegnaButton);
            this.Controls.Add(this.pulisciListaButton);
            this.Controls.Add(this.listinoProdotti);
            this.Controls.Add(this.selectedLabel);
            this.Controls.Add(this.dettagliGridView);
            this.Controls.Add(this.listinoGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "InserisciOrdine";
            this.Text = "whitePaper | Inserisci un nuovo ordine";
            ((System.ComponentModel.ISupportInitialize)(this.listinoGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dettagliGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView listinoGridView;
        private System.Windows.Forms.DataGridView dettagliGridView;
        private System.Windows.Forms.Label selectedLabel;
        private System.Windows.Forms.Label listinoProdotti;
        private System.Windows.Forms.Button pulisciListaButton;
        private System.Windows.Forms.Button consegnaButton;
        private System.Windows.Forms.Button pagamentoButton;
        private System.Windows.Forms.Button confermaOrdineButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label priceLabel;
    }
}