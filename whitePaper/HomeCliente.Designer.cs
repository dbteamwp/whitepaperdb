﻿namespace whitePaper
{
    partial class HomeCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientNameLabel = new System.Windows.Forms.Label();
            this.nuovoOrdineButton = new System.Windows.Forms.Button();
            this.visualizzaOrdiniButton = new System.Windows.Forms.Button();
            this.gestisciDataButton = new System.Windows.Forms.Button();
            this.visualizzaPagamentiButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // clientNameLabel
            // 
            this.clientNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clientNameLabel.Location = new System.Drawing.Point(0, 0);
            this.clientNameLabel.Name = "clientNameLabel";
            this.clientNameLabel.Size = new System.Drawing.Size(1116, 495);
            this.clientNameLabel.TabIndex = 0;
            this.clientNameLabel.Text = "Benvenuto, <cliente>";
            this.clientNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // nuovoOrdineButton
            // 
            this.nuovoOrdineButton.Location = new System.Drawing.Point(12, 83);
            this.nuovoOrdineButton.Name = "nuovoOrdineButton";
            this.nuovoOrdineButton.Size = new System.Drawing.Size(550, 196);
            this.nuovoOrdineButton.TabIndex = 1;
            this.nuovoOrdineButton.Text = "Inserisci un nuovo ordine";
            this.nuovoOrdineButton.UseVisualStyleBackColor = true;
            this.nuovoOrdineButton.Click += new System.EventHandler(this.nuovoOrdineButton_Click);
            // 
            // visualizzaOrdiniButton
            // 
            this.visualizzaOrdiniButton.Location = new System.Drawing.Point(568, 83);
            this.visualizzaOrdiniButton.Name = "visualizzaOrdiniButton";
            this.visualizzaOrdiniButton.Size = new System.Drawing.Size(536, 196);
            this.visualizzaOrdiniButton.TabIndex = 2;
            this.visualizzaOrdiniButton.Text = "Visualizza i tuoi ordini";
            this.visualizzaOrdiniButton.UseVisualStyleBackColor = true;
            this.visualizzaOrdiniButton.Click += new System.EventHandler(this.visualizzaOrdiniButton_Click);
            // 
            // gestisciDataButton
            // 
            this.gestisciDataButton.Location = new System.Drawing.Point(568, 285);
            this.gestisciDataButton.Name = "gestisciDataButton";
            this.gestisciDataButton.Size = new System.Drawing.Size(536, 196);
            this.gestisciDataButton.TabIndex = 4;
            this.gestisciDataButton.Text = "Gestisci i tuoi dati";
            this.gestisciDataButton.UseVisualStyleBackColor = true;
            this.gestisciDataButton.Click += new System.EventHandler(this.gestisciDataButton_Click);
            // 
            // visualizzaPagamentiButton
            // 
            this.visualizzaPagamentiButton.Location = new System.Drawing.Point(12, 285);
            this.visualizzaPagamentiButton.Name = "visualizzaPagamentiButton";
            this.visualizzaPagamentiButton.Size = new System.Drawing.Size(550, 196);
            this.visualizzaPagamentiButton.TabIndex = 3;
            this.visualizzaPagamentiButton.Text = "Visualizza pagamenti";
            this.visualizzaPagamentiButton.UseVisualStyleBackColor = true;
            this.visualizzaPagamentiButton.Click += new System.EventHandler(this.visualizzaPagamentiButton_Click);
            // 
            // HomeCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 495);
            this.Controls.Add(this.gestisciDataButton);
            this.Controls.Add(this.visualizzaPagamentiButton);
            this.Controls.Add(this.visualizzaOrdiniButton);
            this.Controls.Add(this.nuovoOrdineButton);
            this.Controls.Add(this.clientNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "HomeCliente";
            this.Text = "Home | Cliente";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label clientNameLabel;
        private System.Windows.Forms.Button nuovoOrdineButton;
        private System.Windows.Forms.Button visualizzaOrdiniButton;
        private System.Windows.Forms.Button gestisciDataButton;
        private System.Windows.Forms.Button visualizzaPagamentiButton;
    }
}