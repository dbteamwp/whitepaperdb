﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class HomeCliente : Form
    {

        private int _idCliente;

        public HomeCliente(string clientName, int idCliente)
        {
            InitializeComponent();
            clientNameLabel.Text = "Benvenuto, " + clientName.Trim();
            _idCliente = idCliente;
        }

        private void nuovoOrdineButton_Click(object sender, EventArgs e)
        {
            new InserisciOrdine(_idCliente).ShowDialog();
        }

        private void gestisciDataButton_Click(object sender, EventArgs e)
        {
            var gestisciDati = new DatiPersonali.GestioneDatiHome(_idCliente);
            Hide();
            gestisciDati.ShowDialog();
            Show();
        }

        private void visualizzaOrdiniButton_Click(object sender, EventArgs e)
        {
            new visualizzaOrdini(this._idCliente, "CLIENTE").ShowDialog();
        }

        private void visualizzaPagamentiButton_Click(object sender, EventArgs e)
        {
            new PagamentiForm(this._idCliente, "C").ShowDialog();
        }
    }
}
