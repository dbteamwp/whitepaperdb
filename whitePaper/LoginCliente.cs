﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class LoginCliente : Form
    {

        public LoginCliente()
        {
            InitializeComponent();
        }

        private void registratiLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            new RegistrazioneCliente().ShowDialog();
            Show();
        }

        private void accediButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(mailBox.Text) || String.IsNullOrWhiteSpace(pwBox.Text))
            {
                MessageBox.Show("È necessario inserire tutti i campi.");
                return;
            }

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var userPresent = from c in ctx.CLIENTEs
                              where c.mail == mailBox.Text
                              && c.password == pwBox.Text
                              select c;

            if (userPresent.Any())
            {
                Hide();
                new HomeCliente(userPresent.First().ragioneSociale, userPresent.First().idCliente).ShowDialog();
                Close();
            }
            else
            {
                this.datiErratiLabel.Visible = true;
            }
        }
    }
}
