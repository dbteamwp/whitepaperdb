﻿namespace whitePaper
{
    partial class DettagliCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.nominativoTb = new System.Windows.Forms.TextBox();
            this.cf_pivaTb = new System.Windows.Forms.TextBox();
            this.nominativoL = new System.Windows.Forms.Label();
            this.mailTb = new System.Windows.Forms.TextBox();
            this.cf_pivaL = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.idClienteL = new System.Windows.Forms.Label();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.nominativoTb, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.cf_pivaTb, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.nominativoL, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.mailTb, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.cf_pivaL, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.idClienteL, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 24);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(765, 225);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "Mail";
            // 
            // nominativoTb
            // 
            this.nominativoTb.Location = new System.Drawing.Point(385, 59);
            this.nominativoTb.Name = "nominativoTb";
            this.nominativoTb.ReadOnly = true;
            this.nominativoTb.Size = new System.Drawing.Size(302, 31);
            this.nominativoTb.TabIndex = 22;
            // 
            // cf_pivaTb
            // 
            this.cf_pivaTb.Location = new System.Drawing.Point(385, 115);
            this.cf_pivaTb.Name = "cf_pivaTb";
            this.cf_pivaTb.ReadOnly = true;
            this.cf_pivaTb.Size = new System.Drawing.Size(302, 31);
            this.cf_pivaTb.TabIndex = 23;
            // 
            // nominativoL
            // 
            this.nominativoL.AutoSize = true;
            this.nominativoL.Location = new System.Drawing.Point(3, 56);
            this.nominativoL.Name = "nominativoL";
            this.nominativoL.Size = new System.Drawing.Size(119, 25);
            this.nominativoL.TabIndex = 19;
            this.nominativoL.Text = "Nominativo";
            // 
            // mailTb
            // 
            this.mailTb.Location = new System.Drawing.Point(385, 171);
            this.mailTb.Name = "mailTb";
            this.mailTb.ReadOnly = true;
            this.mailTb.Size = new System.Drawing.Size(302, 31);
            this.mailTb.TabIndex = 24;
            // 
            // cf_pivaL
            // 
            this.cf_pivaL.AutoSize = true;
            this.cf_pivaL.Location = new System.Drawing.Point(3, 112);
            this.cf_pivaL.Name = "cf_pivaL";
            this.cf_pivaL.Size = new System.Drawing.Size(93, 25);
            this.cf_pivaL.TabIndex = 21;
            this.cf_pivaL.Text = "CF/PIVA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(202, 37);
            this.label7.TabIndex = 7;
            this.label7.Text = "CLIENTE ID";
            // 
            // idClienteL
            // 
            this.idClienteL.AutoSize = true;
            this.idClienteL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idClienteL.Location = new System.Drawing.Point(385, 0);
            this.idClienteL.Name = "idClienteL";
            this.idClienteL.Size = new System.Drawing.Size(104, 37);
            this.idClienteL.TabIndex = 8;
            this.idClienteL.Text = "NULL";
            // 
            // DettagliCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 262);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "DettagliCliente";
            this.Text = "Dettagli cliente";
            this.Load += new System.EventHandler(this.DettagliCliente_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox nominativoTb;
        private System.Windows.Forms.TextBox cf_pivaTb;
        private System.Windows.Forms.Label nominativoL;
        private System.Windows.Forms.TextBox mailTb;
        private System.Windows.Forms.Label cf_pivaL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label idClienteL;
    }
}