﻿namespace whitePaper.InserimentoOrdini
{
    partial class CompletaOrdine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scadenzaDateTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.subtotaleLabel = new System.Windows.Forms.Label();
            this.spedizioneLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.totaleOrdineLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dettaglioGridView = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.fatturazioneComboBox = new System.Windows.Forms.ComboBox();
            this.inoltraOrdineButton = new System.Windows.Forms.Button();
            this.nuovoIndirizzoButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dettaglioGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // scadenzaDateTime
            // 
            this.scadenzaDateTime.Checked = false;
            this.scadenzaDateTime.Location = new System.Drawing.Point(683, 770);
            this.scadenzaDateTime.Name = "scadenzaDateTime";
            this.scadenzaDateTime.Size = new System.Drawing.Size(362, 31);
            this.scadenzaDateTime.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 769);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(567, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data entro cui vuoi che il tuo ordine sia evaso:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.subtotaleLabel);
            this.groupBox1.Controls.Add(this.spedizioneLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.totaleOrdineLabel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dettaglioGridView);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1261, 748);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Riepilogo ordine";
            // 
            // subtotaleLabel
            // 
            this.subtotaleLabel.AutoSize = true;
            this.subtotaleLabel.Location = new System.Drawing.Point(1102, 595);
            this.subtotaleLabel.Name = "subtotaleLabel";
            this.subtotaleLabel.Size = new System.Drawing.Size(92, 31);
            this.subtotaleLabel.TabIndex = 8;
            this.subtotaleLabel.Text = "label7";
            // 
            // spedizioneLabel
            // 
            this.spedizioneLabel.AutoSize = true;
            this.spedizioneLabel.Location = new System.Drawing.Point(1102, 635);
            this.spedizioneLabel.Name = "spedizioneLabel";
            this.spedizioneLabel.Size = new System.Drawing.Size(92, 31);
            this.spedizioneLabel.TabIndex = 7;
            this.spedizioneLabel.Text = "label6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(929, 635);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 31);
            this.label5.TabIndex = 6;
            this.label5.Text = "Spedizione:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(929, 595);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 31);
            this.label4.TabIndex = 5;
            this.label4.Text = "Subtotale:";
            // 
            // totaleOrdineLabel
            // 
            this.totaleOrdineLabel.AutoSize = true;
            this.totaleOrdineLabel.Location = new System.Drawing.Point(1102, 685);
            this.totaleOrdineLabel.Name = "totaleOrdineLabel";
            this.totaleOrdineLabel.Size = new System.Drawing.Size(54, 31);
            this.totaleOrdineLabel.TabIndex = 4;
            this.totaleOrdineLabel.Text = "0 €";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(929, 685);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "TOTALE:";
            // 
            // dettaglioGridView
            // 
            this.dettaglioGridView.AllowUserToAddRows = false;
            this.dettaglioGridView.AllowUserToDeleteRows = false;
            this.dettaglioGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dettaglioGridView.Location = new System.Drawing.Point(6, 37);
            this.dettaglioGridView.Name = "dettaglioGridView";
            this.dettaglioGridView.ReadOnly = true;
            this.dettaglioGridView.RowTemplate.Height = 33;
            this.dettaglioGridView.Size = new System.Drawing.Size(1249, 535);
            this.dettaglioGridView.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 816);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(302, 31);
            this.label3.TabIndex = 3;
            this.label3.Text = "Indirizzo di fatturazione:";
            // 
            // fatturazioneComboBox
            // 
            this.fatturazioneComboBox.FormattingEnabled = true;
            this.fatturazioneComboBox.Location = new System.Drawing.Point(683, 819);
            this.fatturazioneComboBox.Name = "fatturazioneComboBox";
            this.fatturazioneComboBox.Size = new System.Drawing.Size(362, 33);
            this.fatturazioneComboBox.TabIndex = 4;
            this.fatturazioneComboBox.SelectedIndexChanged += new System.EventHandler(this.fatturazioneComboBox_SelectedIndexChanged);
            // 
            // inoltraOrdineButton
            // 
            this.inoltraOrdineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inoltraOrdineButton.Location = new System.Drawing.Point(935, 947);
            this.inoltraOrdineButton.Name = "inoltraOrdineButton";
            this.inoltraOrdineButton.Size = new System.Drawing.Size(326, 100);
            this.inoltraOrdineButton.TabIndex = 5;
            this.inoltraOrdineButton.Text = "Inoltra ordine";
            this.inoltraOrdineButton.UseVisualStyleBackColor = true;
            this.inoltraOrdineButton.Click += new System.EventHandler(this.inoltraOrdineButton_Click);
            // 
            // nuovoIndirizzoButton
            // 
            this.nuovoIndirizzoButton.Location = new System.Drawing.Point(1084, 809);
            this.nuovoIndirizzoButton.Name = "nuovoIndirizzoButton";
            this.nuovoIndirizzoButton.Size = new System.Drawing.Size(131, 51);
            this.nuovoIndirizzoButton.TabIndex = 6;
            this.nuovoIndirizzoButton.Text = "Nuovo...";
            this.nuovoIndirizzoButton.UseVisualStyleBackColor = true;
            this.nuovoIndirizzoButton.Click += new System.EventHandler(this.nuovoIndirizzoButton_Click);
            // 
            // CompletaOrdine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1273, 1059);
            this.Controls.Add(this.nuovoIndirizzoButton);
            this.Controls.Add(this.inoltraOrdineButton);
            this.Controls.Add(this.fatturazioneComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.scadenzaDateTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CompletaOrdine";
            this.Text = "Completa ordine";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dettaglioGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker scadenzaDateTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label totaleOrdineLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dettaglioGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox fatturazioneComboBox;
        private System.Windows.Forms.Button inoltraOrdineButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label subtotaleLabel;
        private System.Windows.Forms.Label spedizioneLabel;
        private System.Windows.Forms.Button nuovoIndirizzoButton;
    }
}