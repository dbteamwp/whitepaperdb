﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.InserimentoOrdini
{
    public partial class ScegliIndirizzo : Form
    {

        public int? ChosenAddressIndex { get; private set; }
        public float? ShippingCost { get; private set; }

        private int _idCliente;

        public ScegliIndirizzo(int idCliente)
        {
            InitializeComponent();

            _idCliente = idCliente;

            DataGridViewButtonColumn selectAddressButtons = new DataGridViewButtonColumn();
            indirizziGridView.Columns.Add(selectAddressButtons);
            selectAddressButtons.HeaderText = "Seleziona indirizzo";
            selectAddressButtons.Text = "Seleziona";
            selectAddressButtons.Name = "selectProductColumn";
            selectAddressButtons.UseColumnTextForButtonValue = true;

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var indirizzi = from i in ctx.INDIRIZZOs
                            where i.idCliente == idCliente
                            where i.Tipo_consegna == '1'
                            select new { i.città, i.via, i.numeroCivico, i.idIndirizzo };
            indirizziGridView.DataSource = indirizzi;

            indirizziGridView.Visible = false;
        }

        private void siConsegnaButton_CheckedChanged(object sender, EventArgs e)
        {
            if (siConsegnaButton.Checked)
            {
                indirizziGridView.Visible = true;
                usaPredefinitoGroupBox.Visible = true;
                confermaButton.Visible = false;
                ShippingCost = (float) 4.99;
            }
            else
            {
                indirizziGridView.Visible = false;
                usaPredefinitoGroupBox.Visible = false;
                confermaButton.Visible = true;
                ShippingCost = null;
            }
        }

        private void indirizziGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView) sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                ChosenAddressIndex = (int) senderGrid.Rows[e.RowIndex].Cells["idIndirizzo"].Value;
                Close();
            }
        }

        private void confermaButton_Click(object sender, EventArgs e)
        {
            if (usaPredefinitoCheckBox.Checked)
            {
                WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
                var predefinito = from i in ctx.INDIRIZZOs
                                  where i.idCliente == _idCliente
                                  where i.Tipo_consegna == '1'
                                  where i.predefinito == '1'
                                  select i;

                if (predefinito.Any())
                {
                    ChosenAddressIndex = predefinito.First().idIndirizzo;
                    Close();
                }
                else
                {
                    MessageBox.Show("Non è presente alcun indirizzo di consegna predefinito!");
                }
            }
            else
            {
                Close();
            }
        }

        private void usaPredefinitoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (usaPredefinitoCheckBox.Checked)
            {
                indirizziGridView.Visible = false;
                confermaButton.Visible = true;
            }
            else
            {
                indirizziGridView.Visible = true;
            }
        }

        private void veloceRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (veloceRadioButton.Checked)
            {
                ShippingCost = (float) 7.99;
            }
        }

        private void standardRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (standardRadioButton.Checked)
            {
                ShippingCost = (float) 4.99;
            }
        }
    }
}
