﻿namespace whitePaper.InserimentoOrdini
{
    partial class ScegliIndirizzo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.indirizziGridView = new System.Windows.Forms.DataGridView();
            this.confermaButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.siConsegnaButton = new System.Windows.Forms.RadioButton();
            this.noConsegnaButton = new System.Windows.Forms.RadioButton();
            this.usaPredefinitoGroupBox = new System.Windows.Forms.GroupBox();
            this.veloceRadioButton = new System.Windows.Forms.RadioButton();
            this.standardRadioButton = new System.Windows.Forms.RadioButton();
            this.usaPredefinitoCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.indirizziGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.usaPredefinitoGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // indirizziGridView
            // 
            this.indirizziGridView.AllowUserToAddRows = false;
            this.indirizziGridView.AllowUserToDeleteRows = false;
            this.indirizziGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.indirizziGridView.Location = new System.Drawing.Point(12, 247);
            this.indirizziGridView.Name = "indirizziGridView";
            this.indirizziGridView.ReadOnly = true;
            this.indirizziGridView.RowTemplate.Height = 33;
            this.indirizziGridView.Size = new System.Drawing.Size(1089, 740);
            this.indirizziGridView.TabIndex = 0;
            this.indirizziGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.indirizziGridView_CellContentClick);
            // 
            // confermaButton
            // 
            this.confermaButton.Location = new System.Drawing.Point(423, 1011);
            this.confermaButton.Name = "confermaButton";
            this.confermaButton.Size = new System.Drawing.Size(220, 80);
            this.confermaButton.TabIndex = 1;
            this.confermaButton.Text = "Conferma";
            this.confermaButton.UseVisualStyleBackColor = true;
            this.confermaButton.Click += new System.EventHandler(this.confermaButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.siConsegnaButton);
            this.groupBox1.Controls.Add(this.noConsegnaButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1089, 107);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Metodo di ritiro";
            // 
            // siConsegnaButton
            // 
            this.siConsegnaButton.AutoSize = true;
            this.siConsegnaButton.Location = new System.Drawing.Point(451, 50);
            this.siConsegnaButton.Name = "siConsegnaButton";
            this.siConsegnaButton.Size = new System.Drawing.Size(413, 29);
            this.siConsegnaButton.TabIndex = 1;
            this.siConsegnaButton.Text = "Desidero venga effettuata la consegna";
            this.siConsegnaButton.UseVisualStyleBackColor = true;
            this.siConsegnaButton.CheckedChanged += new System.EventHandler(this.siConsegnaButton_CheckedChanged);
            // 
            // noConsegnaButton
            // 
            this.noConsegnaButton.AutoSize = true;
            this.noConsegnaButton.Checked = true;
            this.noConsegnaButton.Location = new System.Drawing.Point(18, 50);
            this.noConsegnaButton.Name = "noConsegnaButton";
            this.noConsegnaButton.Size = new System.Drawing.Size(427, 29);
            this.noConsegnaButton.TabIndex = 0;
            this.noConsegnaButton.TabStop = true;
            this.noConsegnaButton.Text = "Voglio ritirare il mio ordine manualmente";
            this.noConsegnaButton.UseVisualStyleBackColor = true;
            // 
            // usaPredefinitoGroupBox
            // 
            this.usaPredefinitoGroupBox.Controls.Add(this.veloceRadioButton);
            this.usaPredefinitoGroupBox.Controls.Add(this.standardRadioButton);
            this.usaPredefinitoGroupBox.Controls.Add(this.usaPredefinitoCheckBox);
            this.usaPredefinitoGroupBox.Location = new System.Drawing.Point(12, 125);
            this.usaPredefinitoGroupBox.Name = "usaPredefinitoGroupBox";
            this.usaPredefinitoGroupBox.Size = new System.Drawing.Size(1089, 116);
            this.usaPredefinitoGroupBox.TabIndex = 3;
            this.usaPredefinitoGroupBox.TabStop = false;
            this.usaPredefinitoGroupBox.Text = "Usa indirizzo predefinito";
            this.usaPredefinitoGroupBox.Visible = false;
            // 
            // veloceRadioButton
            // 
            this.veloceRadioButton.AutoSize = true;
            this.veloceRadioButton.Location = new System.Drawing.Point(573, 65);
            this.veloceRadioButton.Name = "veloceRadioButton";
            this.veloceRadioButton.Size = new System.Drawing.Size(293, 29);
            this.veloceRadioButton.TabIndex = 5;
            this.veloceRadioButton.TabStop = true;
            this.veloceRadioButton.Text = "Spedizione veloce (7.99€)";
            this.veloceRadioButton.UseVisualStyleBackColor = true;
            this.veloceRadioButton.CheckedChanged += new System.EventHandler(this.veloceRadioButton_CheckedChanged);
            // 
            // standardRadioButton
            // 
            this.standardRadioButton.AutoSize = true;
            this.standardRadioButton.Checked = true;
            this.standardRadioButton.Location = new System.Drawing.Point(573, 30);
            this.standardRadioButton.Name = "standardRadioButton";
            this.standardRadioButton.Size = new System.Drawing.Size(314, 29);
            this.standardRadioButton.TabIndex = 4;
            this.standardRadioButton.TabStop = true;
            this.standardRadioButton.Text = "Spedizione standard (4.99€)";
            this.standardRadioButton.UseVisualStyleBackColor = true;
            this.standardRadioButton.CheckedChanged += new System.EventHandler(this.standardRadioButton_CheckedChanged);
            // 
            // usaPredefinitoCheckBox
            // 
            this.usaPredefinitoCheckBox.AutoSize = true;
            this.usaPredefinitoCheckBox.Location = new System.Drawing.Point(18, 54);
            this.usaPredefinitoCheckBox.Name = "usaPredefinitoCheckBox";
            this.usaPredefinitoCheckBox.Size = new System.Drawing.Size(491, 29);
            this.usaPredefinitoCheckBox.TabIndex = 0;
            this.usaPredefinitoCheckBox.Text = "Usa il mio indirizzo predefinito per la consegna";
            this.usaPredefinitoCheckBox.UseVisualStyleBackColor = true;
            this.usaPredefinitoCheckBox.CheckedChanged += new System.EventHandler(this.usaPredefinitoCheckBox_CheckedChanged);
            // 
            // ScegliIndirizzo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1113, 1116);
            this.Controls.Add(this.usaPredefinitoGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.confermaButton);
            this.Controls.Add(this.indirizziGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ScegliIndirizzo";
            this.Text = "Scegli indirizzo";
            ((System.ComponentModel.ISupportInitialize)(this.indirizziGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.usaPredefinitoGroupBox.ResumeLayout(false);
            this.usaPredefinitoGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView indirizziGridView;
        private System.Windows.Forms.Button confermaButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton siConsegnaButton;
        private System.Windows.Forms.RadioButton noConsegnaButton;
        private System.Windows.Forms.GroupBox usaPredefinitoGroupBox;
        private System.Windows.Forms.CheckBox usaPredefinitoCheckBox;
        private System.Windows.Forms.RadioButton veloceRadioButton;
        private System.Windows.Forms.RadioButton standardRadioButton;
    }
}