﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper.InserimentoOrdini
{
    public partial class CompletaOrdine : Form
    {

        private int _idCliente;
        private int _numeroPagamenti;
        private int? _idIndirizzoConsegna;
        private int? _idFatturazione;
        private DataTable _dettaglio;
        private float _totale;
        private float _spedizione;
        
        public CompletaOrdine(int idCliente, int numeroPagamenti, int? idIndirizzoConsegna, DataTable dettaglio, float totale, float spedizione)
        {
            InitializeComponent();
            scadenzaDateTime.MinDate = DateTime.Today.AddDays(1);

            _idCliente = idCliente;
            _numeroPagamenti = numeroPagamenti;
            _idIndirizzoConsegna = idIndirizzoConsegna;
            _dettaglio = dettaglio;
            _spedizione = spedizione;

            subtotaleLabel.Text = totale + " €";
            spedizioneLabel.Text = spedizione + " €";
            _totale = totale + spedizione;

            dettaglioGridView.DataSource = dettaglio;
            totaleOrdineLabel.Text = _totale + " €";

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var fatturazione = from i in ctx.INDIRIZZOs
                               where i.idCliente == idCliente
                               where i.Tipo_fatturazione == '1'
                               select i;

            if (fatturazione.Any())
            {
                var i = 0;
                foreach (var indirizzo in fatturazione)
                {
                    fatturazioneComboBox.Items.Insert(i++, indirizzo.città.Trim() + ", " + indirizzo.via.Trim() + ", " + indirizzo.numeroCivico.Trim());
                }
            }
        }

        private void fatturazioneComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedAddress = fatturazioneComboBox.SelectedItem.ToString();
            string[] values = selectedAddress.Split(new char[] { ',' });

            var index = 0;
            foreach (var value in values)
            {
                values[index++] = value.Trim();
            }

            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var matchingAddress = from i in ctx.INDIRIZZOs
                                  where i.città == values[0]
                                  where i.via == values[1]
                                  where i.numeroCivico == values[2]
                                  where i.idCliente == _idCliente
                                  select i;

            if (matchingAddress.Any())
            {
                _idFatturazione = matchingAddress.First().idIndirizzo;
            }
        }

        // +++ GOD BUTTON +++
        private void inoltraOrdineButton_Click(object sender, EventArgs e)
        {

            if (fatturazioneComboBox.SelectedItem == null)
            {
                MessageBox.Show("Selezionare un indirizzo di fatturazione.");
                return;
            }

            var ctx = new WhitePaperDataClassesDataContext();
            ORDINE ordine = new ORDINE();
            ordine.idOrdine = (ctx.ORDINEs.Select(o => (int?)o.idOrdine).Max() ?? 0) + 1;
            ordine.dataInserimento = DateTime.Today;
            ordine.dataScadenzaEvasione = scadenzaDateTime.Value;
            ordine.dataEvasione = null;
            ordine.dataRitiro = null;
            ordine.stato = "IN ATTESA";
            ordine.idCliente = _idCliente;
            ctx.ORDINEs.InsertOnSubmit(ordine);
            ctx.SubmitChanges();

            foreach (DataRow row in _dettaglio.Rows)
            {
                DETTAGLIO_ORDINE dettaglio = new DETTAGLIO_ORDINE();
                dettaglio.idDettaglio = (ctx.DETTAGLIO_ORDINEs.Select(d => (int?)d.idDettaglio).Max() ?? 0) + 1;
                dettaglio.quantità = (int) row["quantità"];
                dettaglio.urlImmagine = (string) row["URL"];
                dettaglio.idOrdine = ordine.idOrdine;
                dettaglio.idStampa = (int) row["id stampa"];
                ctx.DETTAGLIO_ORDINEs.InsertOnSubmit(dettaglio);
                ctx.SubmitChanges();
            }

            FATTURA fattura = new FATTURA();
            
            fattura.idFattura = (ctx.FATTURAs.Select(f => (int?)f.idFattura).Max() ?? 0) + 1;
            fattura.idOrdine = ordine.idOrdine;
            fattura.dataEmissione = DateTime.Today;
            fattura.importo = _totale + _spedizione;
            fattura.idIndirizzo = _idFatturazione.Value;
            ctx.FATTURAs.InsertOnSubmit(fattura);
            ctx.SubmitChanges();

            List<PAGAMENTO> payments = new List<PAGAMENTO>();
            var paymentStartingIndex = (ctx.PAGAMENTOs.Select(p => (int?)p.codicePagamento).Max() ?? 0) + 1;
            for (var n = 0; n < _numeroPagamenti; n++)
            {
                PAGAMENTO pagamento = new PAGAMENTO();
                pagamento.idFattura = fattura.idFattura;
                pagamento.codicePagamento = paymentStartingIndex++;
                pagamento.importo = (_totale) / _numeroPagamenti;
                pagamento.dataScadenza = DateTime.Today.AddDays(20 * (n + 1));
                pagamento.dataPagamento = null;
                pagamento.tipoPagamento = null;

                // ctx.PAGAMENTOs.InsertOnSubmit(pagamento);
                payments.Add(pagamento);
            }
            ctx.PAGAMENTOs.InsertAllOnSubmit(payments);
            ctx.SubmitChanges();

            if (_idIndirizzoConsegna.HasValue)
            {
                SPEDIZIONE spedizione = new SPEDIZIONE();
                spedizione.idSpedizione = (ctx.SPEDIZIONEs.Select(s => (int?)s.idSpedizione).Max() ?? 0) + 1;
                spedizione.idOrdine = ordine.idOrdine;
                spedizione.costoSpedizione = _spedizione;
                spedizione.idIndirizzo = _idIndirizzoConsegna.Value;
                spedizione.idCorriere = null;
                ctx.SPEDIZIONEs.InsertOnSubmit(spedizione);
                ctx.SubmitChanges();
            }

            Close();
        }

        private void nuovoIndirizzoButton_Click(object sender, EventArgs e)
        {
            var nuovoIndirizzo = new DatiPersonali.AggiungiIndirizzo(this, _idCliente);
            nuovoIndirizzo.ShowDialog();
            WhitePaperDataClassesDataContext ctx = new WhitePaperDataClassesDataContext();
            var fatturazione = from i in ctx.INDIRIZZOs
                               where i.idCliente == _idCliente
                               where i.Tipo_fatturazione == '1'
                               select i;

            if (fatturazione.Any())
            {
                var i = 0;
                fatturazioneComboBox.Items.Clear();
                foreach (var indirizzo in fatturazione)
                {
                    fatturazioneComboBox.Items.Insert(i++, indirizzo.città.Trim() + ", " + indirizzo.via.Trim() + ", " + indirizzo.numeroCivico.Trim());
                }
            }
        }
    }
}
