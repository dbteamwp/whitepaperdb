﻿namespace whitePaper
{
    partial class ScegliModPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numeroPagamentiBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.prezzoRataLabel = new System.Windows.Forms.Label();
            this.confermaButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numeroPagamentiBox)).BeginInit();
            this.SuspendLayout();
            // 
            // numeroPagamentiBox
            // 
            this.numeroPagamentiBox.Location = new System.Drawing.Point(496, 35);
            this.numeroPagamentiBox.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numeroPagamentiBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeroPagamentiBox.Name = "numeroPagamentiBox";
            this.numeroPagamentiBox.Size = new System.Drawing.Size(120, 31);
            this.numeroPagamentiBox.TabIndex = 0;
            this.numeroPagamentiBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeroPagamentiBox.ValueChanged += new System.EventHandler(this.numeroPagamentiBox_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(410, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Inserisci il numero di pagamenti (max.15):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(223, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ogni rata vale:";
            // 
            // prezzoRataLabel
            // 
            this.prezzoRataLabel.AutoSize = true;
            this.prezzoRataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezzoRataLabel.Location = new System.Drawing.Point(397, 86);
            this.prezzoRataLabel.Name = "prezzoRataLabel";
            this.prezzoRataLabel.Size = new System.Drawing.Size(51, 31);
            this.prezzoRataLabel.TabIndex = 3;
            this.prezzoRataLabel.Text = "0 €";
            // 
            // confermaButton
            // 
            this.confermaButton.Location = new System.Drawing.Point(228, 164);
            this.confermaButton.Name = "confermaButton";
            this.confermaButton.Size = new System.Drawing.Size(223, 89);
            this.confermaButton.TabIndex = 4;
            this.confermaButton.Text = "Conferma";
            this.confermaButton.UseVisualStyleBackColor = true;
            this.confermaButton.Click += new System.EventHandler(this.confermaButton_Click);
            // 
            // ScegliModPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 293);
            this.Controls.Add(this.confermaButton);
            this.Controls.Add(this.prezzoRataLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numeroPagamentiBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ScegliModPagamento";
            this.Text = "Pagamenti";
            ((System.ComponentModel.ISupportInitialize)(this.numeroPagamentiBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numeroPagamentiBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label prezzoRataLabel;
        private System.Windows.Forms.Button confermaButton;
    }
}