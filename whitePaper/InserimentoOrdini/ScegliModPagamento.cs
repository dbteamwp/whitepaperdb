﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace whitePaper
{
    public partial class ScegliModPagamento : Form
    {

        private float _prezzoOrdine;
        public int? NumeroPagamenti { get; private set; }

        public ScegliModPagamento(float prezzoOrdine)
        {
            InitializeComponent();
            _prezzoOrdine = prezzoOrdine;
            prezzoRataLabel.Text = prezzoOrdine + " €";
        }

        private void numeroPagamentiBox_ValueChanged(object sender, EventArgs e)
        {
            prezzoRataLabel.Text = _prezzoOrdine / (float) numeroPagamentiBox.Value + " €";
        }

        private void confermaButton_Click(object sender, EventArgs e)
        {
            NumeroPagamenti = (int) numeroPagamentiBox.Value;
            Close();
        }
    }
}
